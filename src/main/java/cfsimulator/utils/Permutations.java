package cfsimulator.utils;

import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.simulator.AgentStorage;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that generate all permutations.
 * Source: http://stackoverflow.com/questions/2920315/permutation-of-array (author: Yevgen Yampolskiy)
 * Does not allow repetitions in input.
 *
 * created by janovsky
 */
public class Permutations {

    private final AgentStorage agentStorage;
    private final Coalition coalition;
    ArrayList<ArrayList<String>> ret;

    public Permutations(Coalition coalition, AgentStorage agentStorage) {
        this.coalition = coalition;
        this.agentStorage = agentStorage;
    }


    public ArrayList<ArrayList<String>> permute(List<String> arr) {
        return permute(arr, 0);
    }

    private ArrayList<ArrayList<String>> permute(List<String> arr, int k) {
        ret = new ArrayList<>();
        permuteRecursive(arr, k);
        return ret;
    }

    private void permuteRecursive(List<String> arr, int k) {
        for (int i = k; i < arr.size(); i++) {
            java.util.Collections.swap(arr, i, k);
            permuteRecursive(arr, k + 1);
            java.util.Collections.swap(arr, k, i);
        }
        if (k == arr.size() - 1) {
            ArrayList<String> list = new ArrayList<>(arr);
            ret.add(list);
        }
    }

    /**
     * @param arr
     * @param k
     * @return true iff all permutations are dominated by the original coalition
     */
    private boolean permuteRecursiveTest(ArrayList<String> arr, int k) {
        for (int i = k; i < arr.size(); i++) {
            java.util.Collections.swap(arr, i, k);
            if (!permuteRecursiveTest(arr, k + 1)) return false;
            java.util.Collections.swap(arr, k, i);
        }
        if (k == arr.size() - 1) {
            StabilityChecker.Domination d = test(arr);
            if (d.equals(StabilityChecker.Domination.DOMINATING)) {
                return false;
            }
        }
        return true;
    }

    private StabilityChecker.Domination test(ArrayList<String> arr) {
        Coalition test = CoalitionFactory.getInstance().createCoalition(arr, agentStorage);

        return StabilityChecker.getDominationStatus(test, coalition, true);
    }

    /**
     * @param list
     * @return true iff all permutations are dominated by the original coalition
     */
    public boolean testPermute(ArrayList list) {
        if (ConfigurationProvider.getInstance().getStringProperty("profitSharing").equals("EqualSharing")) {
            // With equal sharing the profit is independent of the ordering
            // Therefore just test this single ordering
            return (!test(list).equals(StabilityChecker.Domination.DOMINATING));
        }
        return permuteRecursiveTest(list, 0);
    }
}