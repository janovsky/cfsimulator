package cfsimulator.utils;

import java.util.ArrayList;

/**
 * Helper class that created various kinds of lists.
 *
 * Created by janovsky on 4/29/16.
 */
public class CollectionsProvider {

    public static ArrayList<Integer> createIntList(int size, int value){
        ArrayList<Integer> list = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            list.add(value);
        }
        return list;
    }

    public static ArrayList<Double> createDoubleList(int size, double value) {
        ArrayList<Double> list = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            list.add(value);
        }
        return list;
    }
}
