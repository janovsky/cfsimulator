package cfsimulator.utils;

import cfsimulator.simulation.agent.InterestVector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Currently used to load two different datasets:
 *
 * Loads energy profile dataset of 370 households
 * dataset downloaded from: https://archive.ics.uci.edu/ml/datasets/ElectricityLoadDiagrams20112014#
 *
 * Loads international trade dataset of 167 countries
 * dataset downloaded from: http://stat.wto.org/StatisticalProgram/WSDBStatProgramSeries.aspx?Language=E
 *
 * Created by janovsky on 4/6/15.
 */
public class DatasetProvider {

    private static DatasetProvider instance;
    private ArrayList<ArrayList<Double>> vectors;
    private int counter;

    protected DatasetProvider() {
    }

    public static DatasetProvider getInstance() {
        if (instance == null) {
            instance = new DatasetProvider();
        }
        return instance;
    }

    public InterestVector createInterestVector() {
        return new InterestVector(vectors.get(counter++ % vectors.size()));
    }

    public void init() {
        String fileName;
        counter = 0;
        if (ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.EnergyPurchasingEvaluationFunction")) {
            fileName = ConfigurationProvider.getInstance().getStringProperty("simulation.evaluation.EnergyPurchasingEvaluationFunction.dataFile");
            readEnergyDataset(fileName);
        } else if (ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction")) {
            fileName = ConfigurationProvider.getInstance().getStringProperty("simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction.dataFile");
            readIntTradeDataset(fileName);
        } else {
            System.out.println("DATASET FOR THE EVALUATION FUNCTION IS NOT AVAILABLE");
            return;
        }

    }

    private void readIntTradeDataset(String fileName) {
        int commoditiesCount = 17;
        int countriesCount = 168; // including World
        File loadProfileFile = new File(fileName);
        ArrayList<ArrayList<Double>> list = new ArrayList<>();
        for (int i = 0; i < countriesCount; i++) {
            list.add(new ArrayList<>());
        }

        try (BufferedReader br = new BufferedReader(new FileReader(loadProfileFile))) {
            String line;
            int countryNum = 0;
            int commodityNum = 0;
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ";");
                //read country name
                st.nextToken();
                //read commodity name
                st.nextToken();
                //read amount in dollars
                list.get(countryNum).add(Double.parseDouble(st.nextToken()));
                if (++commodityNum == commoditiesCount) {
                    countryNum++;
                    commodityNum = 0;
                }
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.vectors = list;
    }

    private void readEnergyDataset(String fileName) {
        File loadProfileFile = new File(fileName);
        ArrayList<ArrayList<Double>> list = new ArrayList<>();
        for (int i = 0; i < 370; i++) {
            list.add(new ArrayList<>());
        }

        try (BufferedReader br = new BufferedReader(new FileReader(loadProfileFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                processLine(line, list);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.vectors = list;
    }

    private void processLine(String line, ArrayList<ArrayList<Double>> list) {
        StringTokenizer st = new StringTokenizer(line, ";");
        //read date and time
        st.nextToken();
        int i = 0;
        while (st.hasMoreTokens()) {
            list.get(i).add(Double.valueOf(st.nextToken()));
            i++;
        }
    }

}
