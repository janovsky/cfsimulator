package cfsimulator.utils;

/**
 * Pair implementation.
 *
 * Created by janovsky on 7/24/16.
 */
public class Pair<T>
{
    public final T first;
    public final T second;

    public Pair(T first, T second)
    {
        this.first = first;
        this.second = second;
    }
}
