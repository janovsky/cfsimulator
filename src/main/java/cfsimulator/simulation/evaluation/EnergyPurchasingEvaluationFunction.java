package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.utils.ConfigurationProvider;

import java.util.List;

/**
 * see: Vinyals et al., 2012: Coalitional energy purchasing in the smart grid
 * returns v(C) = sum(qs(C) * ps) + T * qf(C) * pf + kappa(C)
 *
 * Created by janovsky on 4/2/15.
 */
public class EnergyPurchasingEvaluationFunction implements EvaluationFunction {
    private double priceForward;
    private double priceSpot;

    @Override
    public double evaluateCoalition(Coalition coalition) {
        InterestVector v = coalition.getAggregateInterestVector();
        InterestVector sorted = v.sortDescending();
        int n = sorted.getSize();
        double posDouble = (priceForward / priceSpot) * n;
        int pos = (int) Math.floor(posDouble);
        double qf = sorted.get(pos);
        double value = 0;
//      add spot market cost
        for (int t = 0; t < n; t++) {
            double q = Math.max(v.get(t) - qf, 0);
            value += q * priceSpot;
        }
//      add forward market cost
        value += n * qf * priceForward;

//      add coalition size cost
        double gamma = ConfigurationProvider.getInstance().getDoubleProperty("evaluationFunction.gamma");
        double kappa;
        if(coalition.getSize() <= ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.gammaOffset")){
            kappa = 0;
        }
        else{
            kappa = -Math.pow(coalition.getSize() - ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.gammaOffset"), gamma);
        }
        value += kappa;

        // add normal distribution to create an incentive for larger coalitions
        if (ConfigurationProvider.getInstance().getBooleanProperty("evaluationFunction.addNormal")) {
            int targetCoalitionSize = ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.targetCoalitionSize");
            int std = ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.targetCoalitionSizeStd");
            double addNormal = Math.pow(coalition.getSize(), gamma) * Math.exp(-(Math.pow(coalition.getSize() - targetCoalitionSize, 2) / (2 * Math.pow(std, 2))));
            value += addNormal;
        }
        return value;
    }

    @Override
    public void init(List args) {
        priceForward = ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.energyPurchasing.priceForward");
        priceSpot = ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.energyPurchasing.priceSpot");
    }
}
