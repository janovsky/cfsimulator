package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;

import java.util.Iterator;
import java.util.List;

/**
 * A simple evaluation function that sums up k-th element of agents' interest vectors.
 *
 * Created by janovsky on 5/2/16.
 */
public class MaxKthSumEvaluationFunction implements EvaluationFunction {
    private int k;

    @Override
    public double evaluateCoalition(Coalition coalition) {
        double kthSum = 0;
        Iterator<Agent> it = coalition.getAgentsIterator();
        while(it.hasNext()){
            kthSum += it.next().getInterestVector().get(k);
        }
        return kthSum;
    }

    @Override
    public void init(List args) {
        k = 0;//(int) args.get(0);
    }
}
