package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.utils.ConfigurationProvider;

import java.util.List;

/**
 * returns value v(C)= |C|*sum(aggregate vector[i]) if |C| < sizeBound otherwise returns 0
 *
 * Created by janovsky on 3/23/15.
 */
public class BoundedSumEvaluationFunction extends SumEvaluationFunction implements EvaluationFunction {

    private Integer sizeBound;

    @Override
    public void init(List args) {
        sizeBound = ConfigurationProvider.getInstance().getIntProperty("simulation.evaluation.BoundedSumEvaluationFunction.sizeBound");
    }

    @Override
    public double evaluateCoalition(Coalition coalition) {
        if (coalition.getSize() > sizeBound) {
            return 0.0;
        }
        return coalition.getSize() * super.evaluateCoalition(coalition);
    }
}
