package cfsimulator.simulation.evaluation;

import org.apache.log4j.Logger;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.simulator.AgentStorage;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.RandomProvider;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * Evaluator agent is a single central agent that is responsible for:
 * evaluating coalitions based on the given Evaluation Function
 * creating evaluations of all possible coalitions
 * posting a winner of each previous iteration
 *
 * Created by pavel on 2/19/15.
 */
public class BasicEvaluatorAgent implements EvaluatorAgent {
    private EvaluationFunction evaluationFunction;
    private AgentStorage agentStorage;
    private int id;
    private ArrayList<String> winners = new ArrayList<>();
    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public double evaluateSingle(Coalition coalition) {
        double ret = evaluationFunction.evaluateCoalition(coalition);
        if (!(evaluationFunction instanceof PrecomputedEvaluationFunction)) {
            ret *= ConfigurationProvider.getInstance().getIntProperty("evaluator.discretizationMultiplier");
            ret = Math.round(ret);
        }
        return ret;
    }

    @Override
    public void evaluateAll(CoalitionStructure coalitionStructure) {
        PriorityQueue<Coalition> queue = new PriorityQueue<>();
        coalitionStructure.getOpenCoalitionMap().forEach((k, v) -> queue.add(v));
        Coalition winner = queue.poll();
        winners.add(winner.getName());
        winnersValues.add(winner.getCoalitionValue());
        logger.debug("winner is: " + winner.getState());
        if (winners.size() > 1) {
            Coalition coalition = coalitionStructure.getOpenCoalitionMap().get(winners.get(winners.size() - 2));
            if (coalition != null) {
                logger.debug("(Last winner is now: " + coalition.getState() + ")");
            } else {
                logger.debug("(Last winner broke up)");
            }
        }
    }

    @Override
    public void init(EvaluationFunction evaluationFunction, AgentStorage agentStorage, int id) {
        this.evaluationFunction = evaluationFunction;
        this.agentStorage = agentStorage;
        this.id = id;
    }

    @Override
    public ArrayList<String> getWinners() {
        return winners;
    }

    @Override
    public ArrayList<Double> getWinnersValues() {
        return winnersValues;
    }

    public void evaluateAllCombinations() {
        String ef = ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction");
        ef = ef.substring(ef.lastIndexOf(".") + 1);
        int seed = ConfigurationProvider.getInstance().getIntProperty("simulation.randomSeed");
        int n = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfAgents");
        File file = new File("dpidpinput/" + ef + "-n" + n + "-s" + seed);
        if (!file.exists()) {

            RandomProvider.getInstance().switchToTemporary();
            Set<Integer> set = new HashSet<>();
            StringBuilder sb = new StringBuilder();

            long counter = 1;
            for (int i = 1; i <= n; i++) {
                set.add(i);
            }
            sb.append("0 #0 {}\n");
            ArrayList<ArrayList<Integer>> powerSet = generateSubsets();
            for (ArrayList<Integer> s : powerSet) {
                if (!s.isEmpty()) {
                    ArrayList<String> l = new ArrayList<>();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("{");
                    int count = 0;
                    for (Integer i : s) {
                        l.add("Agent" + i);
                        sb2.append(i);
                        if (++count < s.size()) {
                            sb2.append(",");
                        }

                    }
                    sb2.append("}\n");
                    Coalition c = CoalitionFactory.getInstance().createCoalition(l, agentStorage);
                    sb.append((int) Math.round(c.getCoalitionValue())).append(" #").append(counter++).append(" ").append(sb2);
                }
            }


            file.getParentFile().mkdirs();
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(file, "UTF-8");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            writer.print(sb);
            writer.close();

            RandomProvider.getInstance().switchToDefault();
            CoalitionFactory.getInstance().reset();
        }
    }

    @Override
    public EvaluationFunction getEvaluationFunction() {
        return evaluationFunction;
    }

    private ArrayList<ArrayList<Integer>> generateSubsets() {
        int n = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfAgents");
        ArrayList<ArrayList<Integer>> subsets = new ArrayList<>();
        int limit = (1 << n);
        for (int i = 0; i < limit; i++) {
            subsets.add(stringBM(i, n));
        }
        return subsets;
    }

    private ArrayList<Integer> stringBM(int bm, int n) {
        int i, mask = 1;
        ArrayList<Integer> list = new ArrayList<>();

        for (i = 1; i < n + 1; i++) {
            mask = 1 << (i - 1);

            if ((mask & bm) != 0) {
                list.add(i);
            }
        }
        return list;
    }
}
