package cfsimulator.simulation.evaluation;

import org.apache.log4j.Logger;
import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.utils.ConfigurationProvider;

import java.util.List;

/**
 * returns v(C) = -sum(aggregate-vector[i] - target[i])
 *
 * Created by janovsky on 3/5/15.
 */
public class DistanceToTargetEvaluationFunction implements EvaluationFunction {

    private InterestVector target;
    private Logger logger = Logger.getLogger(getClass());

    @Override
    public double evaluateCoalition(Coalition coalition) {
        InterestVector iv = coalition.getAggregateInterestVector();
        double ret = -iv.manhattanDistance(target);
        return ret;

    }

    @Override
    public void init(List args) {
        if (target == null) {
            target = new InterestVector(ConfigurationProvider.getInstance().getDoubleProperty("simulation.evaluation.DistanceToTargetEvaluationFunction.const"), ConfigurationProvider.getInstance().getIntProperty("agent.interestVectorSize"));
            logger.info("target: " + target);
        }

    }

}
