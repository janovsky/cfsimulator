package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.utils.ConfigurationProvider;

/**
 * returns v(C) = sum(min(b+[i], b-[i])) + kappa(C)
 * b+ .. vector of surpluses
 * b- .. vector of shortages
 * kappa .. coalition size penalty
 * v(C) represents the amount of resources that can be shared within the coalition
 *
 * Created by janovsky on 2/16/15.
 */
public class MarketBasedSizePenalizedEvaluationFunction extends MarketBasedEvaluationFunction {

    @Override
    public double evaluateCoalition(Coalition coalition) {
        double unchargedValue = super.evaluateCoalition(coalition);
        double gamma = ConfigurationProvider.getInstance().getDoubleProperty("evaluationFunction.gamma");
        double kappa;
        if(coalition.getSize() <= ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.gammaOffset")){
            kappa = 0;
        }
        else{
            kappa = -Math.pow(coalition.getSize() - ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.gammaOffset"), gamma);
        }
        // add normal distribution to create an incentive for larger coalitions
        if (ConfigurationProvider.getInstance().getBooleanProperty("evaluationFunction.addNormal")) {
            int targetCoalitionSize = ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.targetCoalitionSize");
            int std = ConfigurationProvider.getInstance().getIntProperty("evaluationFunction.targetCoalitionSizeStd");
            double addNormal = Math.pow(coalition.getSize(), gamma) * Math.exp(-(Math.pow(coalition.getSize() - targetCoalitionSize, 2) / (2 * Math.pow(std, 2))));
            unchargedValue += addNormal;
        }

        return Math.max(unchargedValue + kappa, 0);
    }


}
