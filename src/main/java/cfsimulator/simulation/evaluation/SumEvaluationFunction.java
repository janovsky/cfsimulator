package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * returns v(C) = sum(aggregate-vector[i])
 *
 * Created by janovsky on 2/13/15.
 */
public class SumEvaluationFunction implements EvaluationFunction {
    @Override
    public double evaluateCoalition(Coalition coalition) {
        double sum = 0;
        Iterator it = coalition.getAgentsMap().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Agent> entry = (Map.Entry) it.next();
            sum += entry.getValue().getInterestVector().sum();
        }
        return sum;
    }

    @Override
    public void init(List args) {

    }

}
