package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.coalition.Coalition;

import java.util.List;

/**
 * f : C -> R
 * assigns a value to a given coalition
 *
 * Created by janovsky on 2/13/15.
 */
public interface EvaluationFunction {

    double evaluateCoalition(Coalition coalition);

    void init(List args);

}
