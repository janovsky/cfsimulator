package cfsimulator.simulation.evaluation;

import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.simulator.AgentStorage;

import java.util.ArrayList;

/**
 * a single central agent that is responsible for evaluating coalitions using a given evaluation function
 *
 * Created by pavel on 2/19/15.
 */
public interface EvaluatorAgent {

    ArrayList<String> winners = new ArrayList<>();
    ArrayList<Double> winnersValues = new ArrayList<>();

    /**
     * evaluate a single coalition
     *
     * @param coalition
     * @return
     */
    double evaluateSingle(Coalition coalition);

    /**
     * should be used when the evaluation of each coalition depends on the evaluation of other coalitions
     * @param coalitionStructure
     */
    void evaluateAll(CoalitionStructure coalitionStructure);

    void init(EvaluationFunction evaluationFunction, AgentStorage agentStorage, int id);

    ArrayList<String> getWinners();

    ArrayList<Double> getWinnersValues();

    void evaluateAllCombinations();

    EvaluationFunction getEvaluationFunction();
}
