package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.ConfigurationProvider;

/**
 * Unused.
 *
 * Created by janovsky on 2/16/15.
 */
@Deprecated
public class LeaveIfLowCoalitionValueStrategy extends RandomStrategy {


    public LeaveIfLowCoalitionValueStrategy(Agent agent) {
        super();
    }

    @Override
    public boolean leaveCoalition(Coalition coalition, CoalitionStructure cs, Agent agent) {
        return (coalition.getCoalitionValue(agent) <= ConfigurationProvider.getInstance().getDoubleProperty("LeaveIfLowCoalitionValueStrategy.threshold"));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
