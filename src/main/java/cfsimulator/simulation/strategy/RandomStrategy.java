package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.RandomProvider;

/**
 * Randomly decides to leave / pick a coalition.
 * Provides fast search of the state-space
 *
 * Created by janovsky on 2/10/15.
 */
public class RandomStrategy implements Strategy {

    @Override
    public boolean leaveCoalition(Coalition coalition, CoalitionStructure cs, Agent agent) {
        if(coalition == null) return true;

        return RandomProvider.getInstance().getRandom().nextBoolean();
    }

    /**
     * @param coalitionStructure
     * @param agent
     * @return random coalition
     */
    @Override
    public Coalition pickCoalition(CoalitionStructure coalitionStructure, Agent agent) {
        boolean coalitionLeaders = ConfigurationProvider.getInstance().getBooleanProperty("simulation.coalitionLeaders");

        // Never stay alone if we have coalition leaders. In that case we have to choose a coalition with a coalition leader
        double stayAlone = coalitionLeaders ? 2 : RandomProvider.getInstance().getRandom().nextDouble();



        //create a new coalition for yourself only
        if (stayAlone < 1.0 / (coalitionStructure.getOpenCoalitionMap().size() + 1)) {
            Coalition newCoalition = CoalitionFactory.getInstance().createCoalition(agent, agent.getAgentStorage());
            return newCoalition;
        }
        Object[] coalitions = coalitionStructure.getOpenCoalitionMap().values().toArray();
        Coalition ret = (Coalition) coalitions[RandomProvider.getInstance().getRandom().nextInt(coalitions.length)];
        if(!coalitionLeaders)
        {
            return ret;
        }
        else
        {
            // only return the coalition if we are actually making some profit in it
            // TEST: we will try to return the coalition anyways, that should significantly
            // increase the exploration of the state space
            return ret;
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
