package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;

/**
 * Local search with random jumps when a local optimum is reached.
 * All agents apply CoalitionValueBasedStrategy as long as the resulting coalition structure keeps changing.
 * When a coalition structure doesn't differ from the one from last iteration, a RandomStrategy is used once
 * by all agents to escape the local optimum.
 *
 * Created by janovsky on 5/1/15.
 */
public class LocalSearchStrategy implements Strategy {

    private CoalitionValueBasedStrategy localSearch;
    private RandomStrategy randomJump;
    private boolean isInitialized = false;

    public void init() {
        localSearch = new CoalitionValueBasedStrategy();
        randomJump = new RandomStrategy();
        isInitialized = true;
    }

    @Override
    public boolean leaveCoalition(Coalition coalition, CoalitionStructure cs, Agent agent) {
        if (!isInitialized) init();
        if(coalition == null) return true;
        if (agent.getAgentStorage().isNoChangeFromLast()) {
            return randomJump.leaveCoalition(coalition, cs, agent);
        } else {
            return localSearch.leaveCoalition(coalition, cs, agent);
        }
    }

    @Override
    public Coalition pickCoalition(CoalitionStructure coalitionStructure, Agent agent) {
        if (!isInitialized) init();
        if (agent.getAgentStorage().isNoChangeFromLast()) {
            return randomJump.pickCoalition(coalitionStructure, agent);
        } else {
            return localSearch.pickCoalition(coalitionStructure, agent);
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
