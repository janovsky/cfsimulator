package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.RandomProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * MixedStrategy can be used to mix any number of strategies with given weights.
 * A deciding strategy is always picked randomly based on the weights of the strategies.
 *
 * Created by janovsky on 2/25/15.
 */
public class MixedStrategy implements Strategy {

    private List<Strategy> strategies;
    private List<Double> weights;
    private List<Double> accumulativeWeights;
    private double sumWeights;

    public void init(List<Strategy> strategies, List<Double> weights) {
        assert (strategies.size() == weights.size());
        this.strategies = strategies;
        this.weights = weights;
        accumulativeWeights = new ArrayList<>();
        sumWeights = 0;
        for (int i = 0; i < weights.size(); i++) {
            sumWeights += weights.get(i);
            accumulativeWeights.add(sumWeights);
        }

    }

    @Override
    public boolean leaveCoalition(Coalition coalition, CoalitionStructure cs, Agent agent) {
        if(coalition == null) return true;

        boolean decision = applyRouletteWheel(strategies).leaveCoalition(coalition, cs, agent);
        return decision;
    }

    private Strategy applyRouletteWheel(List<Strategy> candidates) {
        double random = sumWeights * RandomProvider.getInstance().getRandom().nextDouble();
        for (int i = 0; i < accumulativeWeights.size(); i++) {
            if (random < accumulativeWeights.get(i)) {
                return candidates.get(i);
            }
        }
        return null;
    }

    @Override
    public Coalition pickCoalition(CoalitionStructure coalitionStructure, Agent agent) {
        Coalition decision = applyRouletteWheel(strategies).pickCoalition(coalitionStructure, agent);
        return decision;
    }

    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName()).append(".");
        for (int i = 0; i < strategies.size(); i++) {
            sb.append(strategies.get(i)).append(".").append(weights.get(i));
        }
        return sb.toString();
    }
}
