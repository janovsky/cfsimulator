package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.coalition.PotentialCoalition;
import cfsimulator.utils.ConfigurationProvider;

import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * Locally optimizing strategy
 *
 * Created by janovsky on 3/23/15.
 */
public class CoalitionValueBasedStrategy implements Strategy {

    private Coalition hint;

    /**
     * leave if not in the coalition that would be picked as currently the best
     *
     * @param coalition
     * @param cs
     * @param agent
     * @return
     */
    @Override
    public boolean leaveCoalition(Coalition coalition, CoalitionStructure cs, Agent agent) {
        if(coalition == null) return true;
        hint = null;
        hint = pickCoalition(cs, agent);
        return hint == null || !hint.equals(coalition);
    }

    /**
     * pick a coalition that will benefit from the addition of this agent (i) the most.
     * C_new = argmax_{C \in CS}(v(C) u i) - v(C))
     * @param coalitionStructure
     * @param agent
     * @return
     */
    @Override
    public Coalition pickCoalition(CoalitionStructure coalitionStructure, Agent agent) {
        Iterator<Coalition> openCoalitionIterator = coalitionStructure.getOpenCoalitionIterator();
        PriorityQueue<PotentialCoalition> queue = new PriorityQueue<>();
        while (openCoalitionIterator.hasNext()) {
            Coalition c = openCoalitionIterator.next();
            PotentialCoalition p = new PotentialCoalition(c, agent);
            queue.add(p);
        }
        PotentialCoalition pBest = queue.poll();
        boolean coalitionLeaders = ConfigurationProvider.getInstance().getBooleanProperty("simulation.coalitionLeaders");
        if(coalitionLeaders)
        {
            // we don't allow agents to be alone, they always have to be with a leader
            // or not be in a coalition at all
            if(pBest.getDeltaValue() > 0)
            {
                return pBest.getCoalition();
            }
            else
            {
                return null;
            }
        }
        Coalition alone = CoalitionFactory.getInstance().createCoalition(agent, agent.getAgentStorage());
        if (pBest == null || alone.getCoalitionValue(agent) > pBest.getDeltaValue()) {
            return alone;
        } else {
            return pBest.getCoalition();
        }
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
