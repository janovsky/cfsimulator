package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.RandomProvider;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Random strategy that keeps track of visited coalitions and avoids them.
 * Empirical results: does not significantly improve the RandomSearchStrategy
 *
 * Created by janovsky on 3/31/15.
 */
public class RandomSearchStrategy extends RandomStrategy implements Strategy {

    private HashSet<String> visitedCoalitions = new HashSet<>();

    @Override
    public Coalition pickCoalition(CoalitionStructure coalitionStructure, Agent agent) {
        visitedCoalitions.add(agent.getCoalition().toNumericallyOrderedString());
        double stayAlone = RandomProvider.getInstance().getRandom().nextDouble();
        //create a new coalition for yourself only
        if (stayAlone < 1.0 / (coalitionStructure.getOpenCoalitionMap().size() + 1)) {
            return stayAlone(agent);
        }
        Iterator<Coalition> openCoalitionIterator = coalitionStructure.getOpenCoalitionIterator();
        ArrayList<Coalition> options = new ArrayList<>();
        while (openCoalitionIterator.hasNext()) {
            Coalition c = openCoalitionIterator.next();
            if (!visitedCoalitions.contains(c.toNumericallyOrderedString())) {
                options.add(c);
            }
        }
        if (options.isEmpty()) {
            if (coalitionStructure.getOpenCoalitionMap().containsKey(agent.getCoalition())) {
                return agent.getCoalition();
            } else {
                return stayAlone(agent);
            }
        }
        return options.get(RandomProvider.getInstance().getRandom().nextInt(options.size()));
    }

    private Coalition stayAlone(Agent agent) {
        Coalition newCoalition = CoalitionFactory.getInstance().createCoalition(agent, agent.getAgentStorage());
        return newCoalition;
    }
}
