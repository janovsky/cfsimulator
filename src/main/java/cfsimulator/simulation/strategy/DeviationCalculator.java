package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.RandomProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 *
 * This is a helper class for the agent deviation calculations.
 * Agent deviation is used to increase coalition stability.
 *
 * Created by janovsky on 10/23/15.
 */
public class DeviationCalculator {

    /**
     * Try adding agents to the deviating sub-coalition starting with agent in the order of increasing
     * marginal contribution of the agents in coalition.
     *
     * @param coalition
     * @param agent
     * @return list of names of agents to deviate with, INCLUDING agent itself
     */
    public static ArrayList<String> deviate(Coalition coalition, Agent agent) {
        if (coalition.getSize() == 1) return null;
        Coalition c = CoalitionFactory.getInstance().createCoalition(agent);
        if (c.getProfit(agent.getName()) < coalition.getProfit(agent.getName()))
            return null;

        ArrayList<String> ret = new ArrayList<>();
        ret.add(agent.getName());
        boolean increasingOrder = ConfigurationProvider.getInstance().getBooleanProperty("agent.strategy.deviate.IncreasingMarginalContribution");
        boolean isRandom = ConfigurationProvider.getInstance().getBooleanProperty("agent.strategy.deviate.RandomOrder");
        ArrayList<String> orderedAgents = orderedByEntryMarginalContribution(coalition, increasingOrder, isRandom);
        for (String otherAgent : orderedAgents) {
            if (otherAgent.equals(agent.getName())) continue;
            ret.add(otherAgent);
            Coalition cNew = CoalitionFactory.getInstance().createCoalition(ret, agent.getAgentStorage());

            if (!ConfigurationProvider.getInstance().getBooleanProperty("simulation.stability.allAgentsMustProfit")) {
                // Decision logic: keep looking while equal profit, return when first positive profit occurs.
                // If encountered negative profit, return null
                if (cNew.getProfit(otherAgent) > coalition.getProfit(otherAgent)) {
                    // we found a sub-coalition where an agent will benefit more while others don't lose anything
                    return ret;
                }
                if (cNew.getProfit(otherAgent) < coalition.getProfit(otherAgent)) {
                    // all agents before don't have any change and this one is losing
                    return null;
                }
            } else {
                // Decision logic: keep going as long as you encounter positive profit
                // If encountered no change or negative profit, don't add this agent and return what we built before it
                if (cNew.getProfit(otherAgent) <= coalition.getProfit(otherAgent)) {
                    if (ret.size() == 2) {
                        // This other agent was the first one we tried and it failed
                        return null;
                    } else {
                        // This is the first agent that failed, remove it and return what we built with previous agents
                        ret.remove(ret.size() - 1);
                        return ret;
                    }
                }
            }
        }
        if (!ConfigurationProvider.getInstance().getBooleanProperty("simulation.stability.allAgentsMustProfit")) {
            // we didn't find any positive change
            return null;
        } else {
            // all agents we went through were positive matches
            return ret;
        }
    }

    private static ArrayList<String> orderedByEntryMarginalContribution(Coalition coalition, boolean increasing, boolean isRandom) {
        ArrayList<String> ret = new ArrayList<>(coalition.getAgentsOrdered());
        if (isRandom) {
            Collections.shuffle(ret, RandomProvider.getInstance().getRandom());
        } else {
            Collections.sort(ret, (o1, o2) -> {
                Double m1 = coalition.getEntryMarginalContribution(o1);
                Double m2 = coalition.getEntryMarginalContribution(o2);
                if (increasing) return m1.compareTo(m2);
                else return m2.compareTo(m1);
            });
        }
        return ret;
    }

    private static ArrayList<String> orderedByCurrentMarginalContribution(Coalition coalition, boolean increasing) {
        ArrayList<String> ret = coalition.getAgentsOrdered();
        Collections.sort(ret, (o1, o2) -> {
            Double m1 = coalition.getCurrentMarginalContribution(o1);
            Double m2 = coalition.getCurrentMarginalContribution(o2);
            if (increasing) return m1.compareTo(m2);
            else return m2.compareTo(m1);
        });
        return ret;
    }

    public static ArrayList<String> deviateGreedy(Coalition coalition, Agent agent) {
        Iterator<Agent> agentsIterator = coalition.getAgentsIterator();
        double max = -Double.MAX_VALUE;
        String agentToAdd = null;
        Coalition cMax = null;
        ArrayList<String> list = new ArrayList<>();
        list.add(agent.getName());
        for (int i = 0; i < coalition.getSize() - 1; i++) {
            while (agentsIterator.hasNext()) {
                Agent a = agentsIterator.next();
                list.add(a.getName());
                Coalition c = CoalitionFactory.getInstance().createCoalition(list, agent.getAgentStorage());
                if (c.getCoalitionValue() > max) {
                    max = c.getCoalitionValue();
                    agentToAdd = a.getName();
                    cMax = c;
                }
            }
            if (cMax.getProfit(agentToAdd) < coalition.getProfit(agentToAdd)) {
                if (list.size() <= 2) return null;
                list.remove(list.size() - 1);
                return list;
            }
        }
        return null;
    }
}
