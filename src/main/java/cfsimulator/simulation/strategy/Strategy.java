package cfsimulator.simulation.strategy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;

import java.util.ArrayList;

/**
 * Strategy is used by an agent to decide whether to leave a coalition and which coalition to pick.
 *
 * Created by janovsky on 2/10/15.
 */
public interface Strategy {

    boolean leaveCoalition(Coalition coalition, CoalitionStructure cs, Agent agent);

    Coalition pickCoalition(CoalitionStructure coalitionStructure, Agent agent);

    String toString();

    default ArrayList<String> deviate(Coalition coalition, Agent agent) {
        return DeviationCalculator.deviate(coalition, agent);
    }

    default void closeCoalition(Coalition coalition, Agent agent)
    {
        // by default do nothing, this can be used by coalition leaders
    }
}
