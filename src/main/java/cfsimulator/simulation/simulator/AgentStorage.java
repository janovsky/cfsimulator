package cfsimulator.simulation.simulator;

import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;
import cfsimulator.renewableEnergy.IEEE69Bus;
import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.evaluation.EvaluationFunction;
import cfsimulator.simulation.evaluation.EvaluatorAgent;
import cfsimulator.simulation.evaluation.MaxKthSumEvaluationFunction;
import cfsimulator.simulation.evaluation.PrecomputedEvaluationFunction;
import cfsimulator.simulation.provider.ProviderAgent;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.RandomProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * AgentStorage stores and updates all the essential data structures: map of agents and map of coalitions.
 *
 * Created by janovsky on 2/10/15.
 */
public class AgentStorage {
    private final CoalitionStructure coalitionStructure;
    /**
     * This map stores a 1x69 voltage vector for each time slot in the IEEE69 scenario.
     * The voltages are for the grid before coalition formation takes place
     */
    public Map<Integer, double[]> initialVoltageMap;
    private java.util.HashMap<String, Agent> agentMap;
    private ArrayList<String> coalitionsToRemove = new ArrayList<>();
    private ProviderAgent providerAgent;
    private InterestVector providersOffer;
    /**
     * evaluatorAgent is used if there is only a single evaluator agent
     */
    private EvaluatorAgent evaluatorAgent;
    /**
     * evaluatorAgentList is used for multiple evaluator agents (agents have heterogenous utilities)
     */
    private ArrayList<EvaluatorAgent> evaluatorAgentList = new ArrayList<>();
    private Logger logger = Logger.getLogger(this.getClass());
    private boolean noChangeFromLast = false;
    private String currentCoalitionStructure = "";
    private IEEE69Bus grid;


    public AgentStorage() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        this.agentMap = new HashMap<>();
        this.coalitionStructure = new CoalitionStructure();

        if(!ConfigurationProvider.getInstance().getBooleanProperty("evaluator.heterogeneousEvaluation")) {

            EvaluationFunction evaluationFunction;

            if (ConfigurationProvider.getInstance().getBooleanProperty("evaluator.parseEvaluations")) {
                evaluationFunction = new PrecomputedEvaluationFunction();
            } else {
                evaluationFunction = (EvaluationFunction) Class.forName(ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction")).newInstance();
            }
            evaluationFunction.init(null);


            this.evaluatorAgent = (EvaluatorAgent) Class.forName(ConfigurationProvider.getInstance().getStringProperty("evaluator.agent")).newInstance();
            evaluatorAgent.init(evaluationFunction, this, 1);


            if (evaluatorAgent instanceof ProviderAgent) {
                ((ProviderAgent) evaluatorAgent).createOffers();
                ((ProviderAgent) evaluatorAgent).showOffers();
            }
        }
        else{
            int categoriesCount = ConfigurationProvider.getInstance().getIntProperty("evaluator.EFCount");
            int interestVectorLength = ConfigurationProvider.getInstance().getIntProperty("agent.interestVectorSize");
            for (int id = 0; id < categoriesCount; id++) {
//                   EvaluationFunction evaluationFunction =
//                           (EvaluationFunction) Class.forName(ConfigurationProvider.getInstance().getStringProperty("evaluator.EvaluatorAgent" + id + ".evaluationFunction")).newInstance();
                    EvaluationFunction evaluationFunction = new MaxKthSumEvaluationFunction();
                if(evaluationFunction instanceof MaxKthSumEvaluationFunction){
                    int k = id / interestVectorLength;
                    ArrayList<Integer> args = new ArrayList<>();
                    args.add(k);
                    evaluationFunction.init(args);
                }
                evaluationFunction.init(null);
                EvaluatorAgent ea = (EvaluatorAgent) Class.forName(ConfigurationProvider.getInstance().getStringProperty("evaluator.agent")).newInstance();
                ea.init(evaluationFunction, this, id);
                evaluatorAgentList.add(ea);
            }
        }

    }

    public void addRandomAgents(int count, boolean participateInCF, boolean coalitionLeaders, boolean energyStores) {
        int categoriesCount = ConfigurationProvider.getInstance().getIntProperty("evaluator.EFCount");
        int stepSize = count / categoriesCount;
        for (int i = 0; i < count; i++) {
            int id = (agentMap.size() + 1);
            Agent a = new Agent(id, this, participateInCF, coalitionLeaders, energyStores);
            agentMap.put(a.getName(), a);
            logger.info(a.getName() + ": " + a.getInterestVector());
            if (ConfigurationProvider.getInstance().getBooleanProperty("evaluator.heterogeneousEvaluation")) {
                int evaluatorAgentID = i / stepSize;
                a.setUtilityTypeID(evaluatorAgentID);
                logger.info(a.getName() + " utility ID: " + evaluatorAgentID);
            }
        }
    }

    public String getState() {
        StringBuilder sb = new StringBuilder();
        agentMap.forEach((k, v) -> sb.append(v.getState()));
        sb.append("\n");
        coalitionStructure.getOpenCoalitionMap().forEach((k, v) -> sb.append(v.getState()));
        return sb.toString();
    }

    public boolean isNoChangeFromLast() {
        return noChangeFromLast;
    }

    public void update(StopWatch timer, int timeLimit) {
        int updateSubsetSize = ConfigurationProvider.getInstance().getIntProperty("simulator.updateSubsetSize");

        // NOTE: nameBasedOrder = true should only be used for debug purposes, as it can give advantage to some agents
        boolean nameBasedOrder = false;
        boolean ieee69Order = ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.priorityOrder");
        ArrayList<String> agentsToUpdate;
        if(!nameBasedOrder){
            agentsToUpdate = ieee69Order ? ieee69Order() : pickAgentsToUpdate(updateSubsetSize);

            for (int i = 0; i < agentsToUpdate.size(); i++) {
                agentMap.get(agentsToUpdate.get(i)).update();
                if (timeLimit > 0 && timer.getTime() > timeLimit) {
                    break;
                }
            }

            // try to deviate as a separate step to increase stability
            boolean deviate = ConfigurationProvider.getInstance().getBooleanProperty("agent.strategy.deviate");
            if (deviate) {
                for (int i = 0; i < agentsToUpdate.size(); i++) {
                    //deviate as long as the agent profits from it
                    boolean deviateRecursively = true;
                    while (deviateRecursively) {
                        deviateRecursively = agentMap.get(agentsToUpdate.get(i)).deviate();
                    }
                    if (timeLimit > 0 && timer.getTime() > timeLimit) {
                        break;
                    }
                }
            }
        }
        else{
            for (int i = 1; i <= getNumberOfAgents(); i++) {
                agentMap.get("Agent" + i).update();
                if (timeLimit > 0 && timer.getTime() > timeLimit) {
                    break;
                }
            }
        }

        coalitionsToRemove.forEach(coalitionStructure::removeCoalition);
        // evaluatorAgent is null in case of heterogeneous evaluations. In that case for now DO NOT evaluate all
        if(evaluatorAgent != null) evaluatorAgent.evaluateAll(coalitionStructure);

        noChangeFromLast = coalitionStructure.toOrderedString().equals(currentCoalitionStructure);
        currentCoalitionStructure = coalitionStructure.toOrderedString();
        if (evaluatorAgent instanceof ProviderAgent) {
            coalitionStructure.sendOffers((ProviderAgent) evaluatorAgent);
            ((ProviderAgent) evaluatorAgent).makeAssignments();
        }
    }

    private ArrayList<String> ieee69Order() {
        ArrayList<Agent> order = new ArrayList<>(agentMap.values());

        order.sort((a1, a2) -> (int)Math.signum(a2.getGridNode().getPriority() - a1.getGridNode().getPriority()));
        ArrayList<String> orderedNames = new ArrayList<>(order.size());
        for(Agent a : order)
        {
            orderedNames.add(a.getName());
        }

        return orderedNames;
    }

    private ArrayList<String> pickAgentsToUpdate(int updateSubsetSize) {
        if (updateSubsetSize == 0) updateSubsetSize = agentMap.size();
        ArrayList<String> ret = new ArrayList<>();
        ArrayList<String> agentNames = new ArrayList<>(agentMap.keySet());

        // sort by name to enforce determinism
        agentNames.sort(String::compareTo);

        for (int i = 0; i < updateSubsetSize; i++) {
            String a = agentNames.get(RandomProvider.getInstance().getAgentStorageRandom().nextInt(agentNames.size()));
            ret.add(a);
            agentNames.remove(a);
            if (agentNames.isEmpty()) return ret;
        }
        return ret;
    }

    public void addCoalitionToRemove(String name) {
        this.coalitionsToRemove.add(name);
    }

    public CoalitionStructure getCoalitionStructure() {
        return coalitionStructure;
    }

    public Iterator<Agent> getAgentsIterator() {
        return agentMap.values().iterator();
    }

    public ProviderAgent getProviderAgent() {
        if (evaluatorAgent instanceof ProviderAgent) {
            return (ProviderAgent) evaluatorAgent;
        } else return null;
    }

    public EvaluatorAgent getEvaluatorAgent() {
        return evaluatorAgent;
    }

    public int getNumberOfAgents() {
        return agentMap.size();
    }

    public Agent getAgent(String agentName) {
        return agentMap.get(agentName);
    }

    public void createEvaluations() {
        evaluatorAgent.evaluateAllCombinations();
    }

    public void InitCoalitions() {
        Iterator<Agent> agentsIterator = getAgentsIterator();
        boolean coalitionLeaders = ConfigurationProvider.getInstance().getBooleanProperty("simulation.coalitionLeaders");

        if (ConfigurationProvider.getInstance().getStringProperty("simulation.initCS").equals(InitialCSSetting.SINGLETONS.toString())) {
            while (agentsIterator.hasNext()) {
                Agent a = agentsIterator.next();
                if(!coalitionLeaders || a.isCoalitionLeader())
                {
                    Coalition c = CoalitionFactory.getInstance().createCoalition(a, this);
                    a.setCoalition(c);
                    coalitionStructure.addCoalition(c);
                }

            }
        } else if (ConfigurationProvider.getInstance().getStringProperty("simulation.initCS").equals(InitialCSSetting.GRAND_COALITION.toString())) {
            ArrayList<String> agentNames = new ArrayList<>();
            while (agentsIterator.hasNext()) {
                Agent a = agentsIterator.next();
                agentNames.add(a.getName());
            }
            Coalition c = CoalitionFactory.getInstance().createCoalition(agentNames, this);
            c.notifyAgents();
            coalitionStructure.addCoalition(c);

        }
    }

    public void InitCoalitionsTEST() {
        Iterator<Agent> agentsIterator = getAgentsIterator();
        Agent aa = agentsIterator.next();
        Coalition c = CoalitionFactory.getInstance().createCoalition(aa, this);
        aa.setCoalition(c);

        while (agentsIterator.hasNext()) {
            Agent a = agentsIterator.next();
            c.addAgent(a);
            a.setCoalition(c);
        }
        coalitionStructure.addCoalition(c);
    }

    public ArrayList<EvaluatorAgent> getEvaluatorAgentList() {
        return evaluatorAgentList;
    }

    /**
     * MUST BE CALLED AFTER THE AGENTS ARE CREATED
     */
    public void initIEEE69Grid() {
        // After the agents are created we can create the grid and assign the grid nodes to the agents
        this.grid = new IEEE69Bus();
        grid.init(agentMap);
    }

    public IEEE69Bus getGrid()
    {
        return grid;
    }


    public enum InitialCSSetting {
        SINGLETONS, GRAND_COALITION
    }
}
