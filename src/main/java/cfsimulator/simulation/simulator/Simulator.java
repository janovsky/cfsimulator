package cfsimulator.simulation.simulator;

import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;

import cfsimulator.renewableEnergy.IEEE69Bus;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.ConfigurationProvider;

import java.util.ArrayList;

/**
 * Simulator updates the AgentStorage in an iterative way until the time limit / iteration count limit is reached.
 *
 * Created by janovsky on 2/10/15.
 */
public class Simulator {
    private AgentStorage agentStorage;
    private Statistics statistics;
    private boolean isInitialized = false;
    private long sleepTime = 0;
    private int iterationCounter = 1;
    private StopWatch timer = new StopWatch();
    private Logger logger;
    private int iteration;
    private int timeLimit = 0;

    public void run(int numberOfIterations) {
        if (isInitialized) {
            logger.info("SIMULATION STARTED");

            statistics.startTimer();
            timer.start();
            iteration = 1;

            if (ConfigurationProvider.getInstance().getBooleanProperty("simulation.runToConvergence")) {
                while (true) {
                    doStep();
                    iteration++;
                    ArrayList<Double> coalitionsValueTimeSeries = statistics.getCoalitionsValueTimeSeries();
                    if (coalitionsValueTimeSeries.size() > 1) {
                        if (coalitionsValueTimeSeries.get(coalitionsValueTimeSeries.size() - 1).equals(coalitionsValueTimeSeries.get(coalitionsValueTimeSeries.size() - 2))) {
                            break;
                        }
                    }
                }
            } else if (ConfigurationProvider.getInstance().getBooleanProperty("simulation.timeLimitEnabled")) {
                timeLimit = ConfigurationProvider.getInstance().getIntProperty("simulation.timeLimit");
                while (timer.getTime() < timeLimit) {
                    doStep();
                    iteration++;
                }
            } else {
                for (int i = 0; i < numberOfIterations; i++) {
                    doStep();
                    iteration++;
                }
            }
            timer.stop();

            logger.info("SIMULATION FINISHED, ELAPSED TIME: " + timer.getTime() + " ms");
            if (ConfigurationProvider.getInstance().getBooleanProperty("simulation.CSStabilityChooser.ON")) {
                CSStabilityChooser csStabilityChooser = new CSStabilityChooser(agentStorage);
                int maxAlpha = ConfigurationProvider.getInstance().getIntProperty("simulation.CSStabilityChooser.maxAlpha");
                CoalitionStructure bestCS = csStabilityChooser.chooseBestSolution(statistics.getCoalitionStructuresList(), maxAlpha);

                statistics.finalReport(bestCS);
            } else {
                statistics.finalReport(null);
            }

        } else {
            throw new IllegalStateException("Simulator not initialized");
        }
    }

    private void doStep() {
        agentStorage.update(timer, timeLimit);
        statistics.update();
        logger.debug("Iteration: " + iterationCounter++);
        logger.debug(statistics.report());
        try {
            timer.suspend();
            Thread.sleep(sleepTime);
            timer.resume();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void init() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        logger = Logger.getLogger(this.getClass());
        agentStorage = new AgentStorage();

        boolean renewableGenerationIEEE69 = ConfigurationProvider.getInstance().getStringProperty("scenario").equals("renewableScenario")
                && ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.IEEE69Grid");

        if (renewableGenerationIEEE69) {
            // SPECIFIC SETTING FOR THE IEEE 69 EXPERIMENTS
            int leaderCount = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfCoalitionLeaders");
            int energyStoreCount = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.energyStoreCount");
            assert (leaderCount + energyStoreCount <= 69);

            // add renewable generators
            agentStorage.addRandomAgents(leaderCount, true, true, false);
            // add energy stores
            agentStorage.addRandomAgents(energyStoreCount, true, false, true);
            // add households
            agentStorage.addRandomAgents(69 - leaderCount - energyStoreCount, false, false, false);

            agentStorage.initIEEE69Grid();

        }
        else {
            // USE THIS FOR ALL EXPERIMENTS EXCEPT FOR IEEE 69
            boolean coalitionLeaders = ConfigurationProvider.getInstance().getBooleanProperty("simulation.coalitionLeaders");
            if (coalitionLeaders) {
                int leaderCount = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfCoalitionLeaders");
                agentStorage.addRandomAgents(leaderCount, true, true, false);
            }

            // This is a general setting where we do not specifically set the energy store flag.
            // However this code is used for the renewable energy scenario without IEEE 69 grid.
            agentStorage.addRandomAgents(ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfAgents"), true, false, false);
        }


        if (ConfigurationProvider.getInstance().getBooleanProperty("evaluator.evaluateAllCombinations")) {
            agentStorage.createEvaluations();
            agentStorage.getEvaluatorAgent().getEvaluationFunction().init(null);
        }
        agentStorage.InitCoalitions();
        statistics = new Statistics(agentStorage);
        isInitialized = true;
    }

    public AgentStorage getAgentStorage() {
        return agentStorage;
    }
}
