package cfsimulator.simulation.simulator;

import com.xeiam.xchart.*;
import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;

import cfsimulator.renewableEnergy.EnergyFinalEvaluator;
import cfsimulator.renewableEnergy.Result;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.StabilityChecker;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Statistics gathers data throughout the simulation that is then written out as the main simulation output.
 *
 * Created by janovsky on 2/13/15.
 */
public class Statistics {
    private final AgentStorage agentStorage;
    private int nextHistogramSnapShot;
    private int iteration;
    private double agentsValue = 0;
    private int numberOfCoalitions;
    private double coalitionsValue;
    private double coalitionValueIndex;
    private ArrayList<Integer> numberOfCoalitionsTimeSeries = new ArrayList<>();
    private double avgCoalitionSize;
    private ArrayList<Double> avgCoalitionSizeTimeSeries = new ArrayList<>();
    private ArrayList<Double> coalitionsValueTimeSeries = new ArrayList<>();
    private double histogramStep;
    private ArrayList<Integer> histogramSnapshotsIterations = new ArrayList<>();
    private int maxCoalitionSize = 0;
    private int histogramBinSize;
    private ArrayList<ArrayList<Integer>> sizesSnapshots = new ArrayList<>();
    private Logger logger = Logger.getLogger(this.getClass());
    private HashSet<String> coalitionStructures = new HashSet<>();
    private StopWatch statisticsTimer = new StopWatch();



    private ArrayList<String> coalitionStructuresList = new ArrayList<>();
    private double maxCoalitionValue = -Double.MAX_VALUE;
    private String bestCS;
    private ArrayList<Double> stabilityTimeSeries = new ArrayList<>();

    private int maxAlpha = ConfigurationProvider.getInstance().getIntProperty("simulation.stabilityChecker.maxAlpha");

    public Statistics(AgentStorage agentStorage) {

        this.agentStorage = agentStorage;

        agentsValue = agentStorage.getCoalitionStructure().getValue();
        logger.info("singleton value: " + agentsValue);
        histogramStep = (double) ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfIterations") / ConfigurationProvider.getInstance().getIntProperty("statistics.histogramSnapshotCount");
        nextHistogramSnapShot = (int) Math.round(histogramStep);
        this.iteration = 0;
        histogramBinSize = ConfigurationProvider.getInstance().getIntProperty("statistics.histogramBinCount");


        update();

        if (ConfigurationProvider.getInstance().getStringProperty("scenario").equals("renewableScenario")
                && ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.IEEE69Grid")) {
            Result resultWithoutCF = new Result(agentStorage.getCoalitionStructure());
            EnergyFinalEvaluator.evaluateGridState(agentStorage.getCoalitionStructure(), agentStorage, resultWithoutCF, false);
            agentStorage.initialVoltageMap = resultWithoutCF.voltageMap;
            agentStorage.getGrid().findPriorities(agentStorage);
        }
    }

    public void update() {
        numberOfCoalitions = agentStorage.getCoalitionStructure().getNumberOfOpenCoalitions();
        numberOfCoalitionsTimeSeries.add(numberOfCoalitions);
        Iterator<Coalition> coalitionsIterator = agentStorage.getCoalitionStructure().getOpenCoalitionIterator();
        coalitionsValue = 0;
        avgCoalitionSize = 0;
        while (coalitionsIterator.hasNext()) {
            Coalition c = coalitionsIterator.next();
            coalitionsValue += c.getCoalitionValue();
            avgCoalitionSize += c.getSize();
        }
        String csString = agentStorage.getCoalitionStructure().toOrderedString();
        if (coalitionsValue > maxCoalitionValue) {
            bestCS = csString;
            maxCoalitionValue = coalitionsValue;
        }
        coalitionValueIndex = coalitionsValue / agentsValue;
        coalitionsValueTimeSeries.add(coalitionsValue);
        avgCoalitionSize /= agentStorage.getCoalitionStructure().getNumberOfOpenCoalitions();
        avgCoalitionSizeTimeSeries.add(avgCoalitionSize);

        if (iteration == nextHistogramSnapShot) {
            nextHistogramSnapShot += Math.round(histogramStep);
            computeHistogramSnapShot();
        }
        coalitionStructures.add(csString);
        coalitionStructuresList.add(csString);

        if (ConfigurationProvider.getInstance().getBooleanProperty("statistics.computeAllStabilities")) {
            StabilityChecker stabilityChecker = new StabilityChecker(agentStorage.getCoalitionStructure(), agentStorage);
            double[] stabilityRatio = stabilityChecker.getStabilityRatio(maxAlpha);
//            logger.info("... finished, strong stability ratio = " + stabilityRatio[0] + ", weak stability ratio = " + stabilityRatio[1]);
            stabilityTimeSeries.add(stabilityRatio[0] + stabilityRatio[1]);
        }

        iteration++;
    }

    private void computeHistogramSnapShot() {
        ArrayList<Integer> sizes = new ArrayList<>();
        Iterator<Coalition> openCoalitionIterator = agentStorage.getCoalitionStructure().getOpenCoalitionIterator();
        while (openCoalitionIterator.hasNext()) {
            int size = openCoalitionIterator.next().getSize();
            sizes.add(size);
            if (size > maxCoalitionSize) {
                maxCoalitionSize = size;
            }
        }
        sizesSnapshots.add(sizes);
        histogramSnapshotsIterations.add(iteration);
    }

    public String report() {
        StringBuilder sb = new StringBuilder();
        sb.append("Number of coalitions: " + numberOfCoalitions).append("; Coalition value: " + coalitionsValue);
        sb.append("\n");
        Iterator<Coalition> openCoalitionIterator = agentStorage.getCoalitionStructure().getOpenCoalitionIterator();
        while (openCoalitionIterator.hasNext()) {
            sb.append(openCoalitionIterator.next().toAgentString()).append(" ");
        }
        return sb.toString();
    }

    /**
     * final report
     *
     * @param bestCS the determined best solution. If unknown, set to null
     */
    public void finalReport(CoalitionStructure bestCS) {
        statisticsTimer.stop();

        experimentReport(bestCS);

        if (ConfigurationProvider.getInstance().getBooleanProperty("statistics.showGraphs")) {
            generateGraphs();
        }
        if (ConfigurationProvider.getInstance().getBooleanProperty("statistics.generateStabilityVsGainData")) {
            generateStabilityVsGainData();
        }


    }


    /**
     * NOTE: the following flags must be turned on in configuration.properties:
     * <p>
     * statistics.generateStabilityVsGainData
     * statistics.computeAllStabilities
     */
    private void generateStabilityVsGainData() {
        StringBuilder sb = new StringBuilder();
        sb.append("CSGain, CSStability \n");
        double gain;
        for (int i = 0; i < coalitionsValueTimeSeries.size(); i++) {
            gain = (coalitionsValueTimeSeries.get(i) - agentsValue) / agentStorage.getNumberOfAgents();
            sb.append(gain).append(", ").append(stabilityTimeSeries.get(i)).append("\n");
        }

        String ef = ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction");
        ef = ef.substring(ef.lastIndexOf(".") + 1);
        String s = agentStorage.getAgent("Agent1").getStrategy().toString();
        String fileName = "process-experiments/stabilityScatter/stabilityVsGainData-" + ef + "-" + s;
        if (ConfigurationProvider.getInstance().getBooleanProperty("agent.strategy.deviate")) {
            fileName = fileName + ".dev";
        } else {
            fileName = fileName + ".nodev";
        }

        File file =
                new File(fileName);
        file.getParentFile().mkdirs();

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(sb);
        writer.close();
    }

    private void generateGraphs() {
        double[] y1Data = new double[numberOfCoalitionsTimeSeries.size()];
        double[] y2Data = new double[numberOfCoalitionsTimeSeries.size()];
        double[] y3Data = new double[numberOfCoalitionsTimeSeries.size()];
        double[] y4Data = new double[numberOfCoalitionsTimeSeries.size()];
        double[] xData = new double[numberOfCoalitionsTimeSeries.size()];

        Iterator<Integer> iterator = numberOfCoalitionsTimeSeries.iterator();
        Iterator<Double> iterator2 = avgCoalitionSizeTimeSeries.iterator();
        Iterator<Double> iterator3 = coalitionsValueTimeSeries.iterator();
        for (int i = 0; i < xData.length; i++) {
            xData[i] = i;
            Integer numberOfCoal = iterator.next();
            y1Data[i] = numberOfCoal;
            y2Data[i] = iterator2.next();
            Double coalValue = (double) Math.round(iterator3.next() * 100000) / 100000;
            y3Data[i] = coalValue;
            y4Data[i] = coalValue / numberOfCoal;
        }

        // Create number of coalitions chart
        Chart chart = new ChartBuilder().width(800).height(300).theme(StyleManager.ChartTheme.Matlab).title("Number of coalitions").xAxisTitle("iteration").yAxisTitle("number of coalitions").build();
        chart.getStyleManager().setPlotGridLinesVisible(false);
        chart.getStyleManager().setXAxisTickMarkSpacingHint(100);
        Series s1 = chart.addSeries("number of coalitions", xData, y1Data);
        s1.setMarker(SeriesMarker.NONE);
        s1.setLineColor(Color.ORANGE);
        chart.getStyleManager().setLegendVisible(false);

        // Create average coalition size chart
        Chart chart2 = new ChartBuilder().width(800).height(300).theme(StyleManager.ChartTheme.Matlab).title("Average coalition size").xAxisTitle("iteration").yAxisTitle("average coalition size").build();
        chart2.getStyleManager().setPlotGridLinesVisible(false);
        chart2.getStyleManager().setXAxisTickMarkSpacingHint(100);
        Series s2 = chart2.addSeries("average coalition size", xData, y2Data);
        s2.setMarker(SeriesMarker.NONE);
        chart2.getStyleManager().setLegendVisible(false);

        // Create Coalition values chart
        Chart chart3 = new ChartBuilder().width(800).height(300).theme(StyleManager.ChartTheme.Matlab).title("Coalition value").xAxisTitle("iteration").yAxisTitle("coalition value").build();
        chart3.getStyleManager().setPlotGridLinesVisible(false);
        chart3.getStyleManager().setXAxisTickMarkSpacingHint(100);
        Series s3 = chart3.addSeries("coalition value", xData, y3Data);
        s3.setLineColor(Color.red);
        s3.setMarkerColor(Color.red);
        chart3.getStyleManager().setLegendVisible(false);
        chart3.getStyleManager().setPlotGridLinesVisible(true);


        // Create Average Coalition values chart
        Chart chart4 = new ChartBuilder().width(800).height(300).theme(StyleManager.ChartTheme.Matlab).title("Average coalition value").xAxisTitle("iteration").yAxisTitle("average coalition value").build();
        chart4.getStyleManager().setPlotGridLinesVisible(false);
        chart4.getStyleManager().setXAxisTickMarkSpacingHint(100);
        Series s4 = chart4.addSeries("average coalition value", xData, y4Data);
        s4.setMarker(SeriesMarker.NONE);
        s4.setLineColor(Color.green);
        chart4.getStyleManager().setLegendVisible(false);

        // Create Histogram of Coalition Sizes
        computeHistogramSnapShot();
        Chart chart5 = new ChartBuilder().chartType(StyleManager.ChartType.Bar).width(800).height(800).title("Coalition size Histogram").xAxisTitle("coalition size").yAxisTitle("count").build();
        int[] sizes = new int[maxCoalitionSize + 1];
        for (int i = 0; i < maxCoalitionSize + 1; i++) {
            sizes[i] = i;
        }
        for (int i = 0; i < histogramSnapshotsIterations.size(); i++) {
            Histogram h = new Histogram(sizesSnapshots.get(i), ConfigurationProvider.getInstance().getIntProperty("statistics.histogramBinCount"), 0, maxCoalitionSize);
            chart5.addSeries("iteration" + histogramSnapshotsIterations.get(i), h.getxAxisData(), h.getyAxisData());
        }
        chart5.getStyleManager().setXAxisMax(maxCoalitionSize + 1);
        chart5.getStyleManager().setXAxisDecimalPattern("#0.0");
        chart5.getStyleManager().setChartBackgroundColor(Color.WHITE);

        // Create stability chart
        Chart chart6 = null;
        if (ConfigurationProvider.getInstance().getBooleanProperty("statistics.computeAllStabilities")) {
            double[] y6Data = new double[numberOfCoalitionsTimeSeries.size()];
            for (int i = 0; i < xData.length; i++) {
                y6Data[i] = stabilityTimeSeries.get(i);
            }
            chart6 = new ChartBuilder().width(800).height(300).theme(StyleManager.ChartTheme.Matlab).title("Stability").xAxisTitle("iteration").yAxisTitle("stability").build();
            chart6.getStyleManager().setPlotGridLinesVisible(false);
            chart6.getStyleManager().setXAxisTickMarkSpacingHint(100);
            Series s6 = chart6.addSeries("stability", xData, y6Data);
            chart6.getStyleManager().setLegendVisible(false);
            chart6.getStyleManager().setPlotGridLinesVisible(true);
        }
        //Add charts to the plot
        List<Chart> l = new ArrayList<>();
        l.add(chart);
        l.add(chart2);
        l.add(chart3);
        l.add(chart4);
        l.add(chart5);
        if (ConfigurationProvider.getInstance().getBooleanProperty("statistics.computeAllStabilities"))
            l.add(chart6);


        new SwingWrapper(l).displayChartMatrix("Coalition Formation Simulator: " + ConfigurationProvider.getInstance().getStringProperty("agent.strategy") + ", " +
                ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction") + ", " +
                ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfAgents") + " agents");
    }

    private void experimentReport(CoalitionStructure bestCS) {
        double avg = 0;
        double max = -Double.MAX_VALUE;
        double last = coalitionsValueTimeSeries.get(coalitionsValueTimeSeries.size() - 1);
        int bestIteration = 0;
        for (int i = 0; i < coalitionsValueTimeSeries.size(); i++) {
            double d = coalitionsValueTimeSeries.get(i);
            avg += d;
            if (d > max) {
                max = d;
                bestIteration = i;
            }
        }

        logger.info("BEST COALITION STRUCTURE:\n" + coalitionStructuresList.get(bestIteration));


        avg /= coalitionsValueTimeSeries.size();

        double avg2 = 0, last2 = 0, max2 = 0;
        if(!ConfigurationProvider.getInstance().getBooleanProperty("evaluator.heterogeneousEvaluation")) {
            ArrayList<Double> winnersValues = agentStorage.getEvaluatorAgent().getWinnersValues();

            avg2 = 0;
            max2 = -Double.MAX_VALUE;
            last2 = winnersValues.get(winnersValues.size() - 1);
            for (double d : winnersValues) {
                avg2 += d;
                if (d > max2) {
                    max2 = d;
                }
            }
            avg2 /= winnersValues.size();
        }
        int seed = ConfigurationProvider.getInstance().getIntProperty("simulation.randomSeed");
        String ef = ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction");
        ef = ef.substring(ef.lastIndexOf(".") + 1);

        int experimentID = ConfigurationProvider.getInstance().getIntProperty("experimentID");
        String sep = " ";
        int firstN = ConfigurationProvider.getInstance().getIntProperty("CoalitionValueBasedStrategy.firstN");
        String s = agentStorage.getAgent("Agent1").getStrategy().toString();//ConfigurationProvider.getInstance().getStringProperty("agent.strategy");
        int n = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfAgents");
        int searchedCSCount = coalitionStructures.size();
        int timeLimit = ConfigurationProvider.getInstance().getIntProperty("simulation.timeLimit");
        double bestFitness = 0;

        double[] stabilityRatio = new double[2];
        if (bestCS != null) {
            max = bestCS.getValue();
            stabilityRatio[0] = bestCS.getAlphaStability();
            bestFitness = bestCS.getFitness();
        }
        double gain = (max - agentsValue);


        // we run the stability checker even if the bestCS was determined i.e. the stabilities were computed for all CSs
        // because the alpha could have been set differently
        if (ConfigurationProvider.getInstance().getBooleanProperty("simulation.stabilityChecker.ON")) {
            logger.info("STABILITY CHECKER ...");
            CoalitionStructure toTest = (bestCS == null) ? (new CoalitionStructure(this.bestCS, agentStorage)) : bestCS;
            StabilityChecker stabilityChecker = new StabilityChecker(toTest, agentStorage);
            stabilityRatio = stabilityChecker.getStabilityRatio(maxAlpha);
            logger.info("... finished, strong stability ratio = " + stabilityRatio[0] + ", weak stability ratio = " + stabilityRatio[1]);
        }
        logger.info("\n experimentID numberOfAgents seed evaluationFunction strategy sumCoalitionValueLast sumCoalitionValueAvg sumCoalitionValueMax " +
                "bestCoalitionValueLast bestCoalitionValueAvg bestCoalitionValueMax singletonValue searchedCSCount iterationCount timeLimit runTime gain strong-stability weak-stability deviation-strategy solutionSelection alphaSolSel alphaStabilityChecker fitness");

        logger.debug("NUMBER OF ITERATIONS: " + coalitionsValueTimeSeries.size());

        String deviationStrategy;
        if (!ConfigurationProvider.getInstance().getBooleanProperty("agent.strategy.deviate")) {
            deviationStrategy = "No-deviation";
        } else {
            if (ConfigurationProvider.getInstance().getBooleanProperty("agent.strategy.deviate.IncreasingMarginalContribution")) {
                deviationStrategy = "increasing-marginal-contribution";
            } else {
                deviationStrategy = "decreasing-marginal-contribution";
            }
        }

        String solutionSelection = ConfigurationProvider.getInstance().getBooleanProperty("simulation.CSStabilityChooser.ON") ? "solution-selection" : "no-solution-selection";
        int alphaSolSel = ConfigurationProvider.getInstance().getIntProperty("simulation.CSStabilityChooser.maxAlpha");

        if(ConfigurationProvider.getInstance().getStringProperty("scenario").equalsIgnoreCase("renewableScenario"))
        {
            Result result = EnergyFinalEvaluator.evaluateAll(this.coalitionStructuresList, agentStorage);
            //this is a special output for the renewable scenario
            double renewableProfitWithCF = result.profit;
            double renewableProfitWithoutCF = result.profitWithoutCF;
            double renewableUsageWithCF = result.renewableGeneration;
            double renewableUsageWithoutCF = result.renewableGenerationWithoutCF;
            double generationProfit = result.generationProfit;
            double unceratintyCost = result.uncertaintyCost;
            double failedToProvideCost = result.failedToProvideCost;
            double generationProfitWithoutCF = result.generationProfitWithoutCF;
            double failedToProvideWithoutCFCost = result.failedToProvideWithoutCFCost;
            double uncertaintyCoverage = ConfigurationProvider.getInstance().getDoubleProperty("renewableScenario.uncertaintyCoverage");
            double commitmentWithoutCF = ConfigurationProvider.getInstance().getDoubleProperty("renewableScenario.commitmentWithoutCF");
            double committedUsedEnergy = result.committedUsedEnergy;

            boolean ieee69GridScenario = ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.IEEE69Grid");
            // number of violations is a difference between situation with and without CF
            int numberOfPosViolationsWithoutCF = ieee69GridScenario ? result.numberOfPosViolationsWithoutCF : 0;
            int numberOfNegViolationsWithoutCF = ieee69GridScenario ? result.numberOfNegViolationsWithoutCF : 0;
            int numberOfPosViolationsWithCF = ieee69GridScenario ? result.numberOfPosViolations : 0;
            int numberOfNegViolationsWithCF = ieee69GridScenario ? result.numberOfNegViolations : 0;
            int numberOfCoalitionLeaders = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfCoalitionLeaders");
            int numberOfEnergyStores = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.energyStoreCount");

            boolean distributeRemainingESPower = ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.distributeRemainingESPower");
            boolean EScanObserveRealGeneration = ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.EScanObserveRealGeneration");
            boolean useRemainingRGPower = ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.useRemainingRGPower");

            double priorityWeight = ConfigurationProvider.getInstance().getDoubleProperty("renewableScenario.priorityWeight");

            double deltaV = result.deltaV;

            int numTimeSlots = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots");

            String esDistributionMethod;
            if(!distributeRemainingESPower)
            {
                esDistributionMethod = "no-distribution";
            }
            else
            {
                if(!EScanObserveRealGeneration)
                {
                    esDistributionMethod = "distribution";
                }
                else
                {
                    esDistributionMethod = "distribution-bound";
                }
            }

            logger.debug("Negative violations:");
            for (int i = 0; i < result.numberOfNegViolationsVector.length; i++) {
                logger.debug(i + ": " + result.numberOfNegViolationsVector[i]);
            }
            logger.debug("Positive violations:");
            for (int i = 0; i < result.numberOfPosViolationsVector.length; i++) {
                logger.debug(i + ": " + result.numberOfPosViolationsVector[i]);
            }


            System.out.println(experimentID + sep + n + sep + seed + sep + ef + sep + s + sep + last + sep + avg + sep + max + sep +
                    last2 + sep + avg2 + sep + max2 + sep + agentsValue + sep + searchedCSCount + sep + iteration + sep + timeLimit + sep + statisticsTimer.getTime() + sep + gain
                    + sep + stabilityRatio[0] + sep + stabilityRatio[1] + sep + deviationStrategy + sep + solutionSelection + sep + alphaSolSel + sep + maxAlpha + sep + bestFitness
                    + sep + renewableProfitWithCF + sep + renewableProfitWithoutCF + sep + renewableUsageWithCF + sep + renewableUsageWithoutCF + sep + generationProfit + sep
                    + unceratintyCost + sep + failedToProvideCost + sep + uncertaintyCoverage
                    + sep + generationProfitWithoutCF + sep + failedToProvideWithoutCFCost + sep + commitmentWithoutCF + sep + committedUsedEnergy + sep + numberOfCoalitionLeaders + sep + numberOfEnergyStores
                    + sep + numberOfPosViolationsWithoutCF + sep + numberOfNegViolationsWithoutCF + sep
                    + numberOfPosViolationsWithCF + sep + numberOfNegViolationsWithCF
                    + sep + esDistributionMethod + sep + useRemainingRGPower + sep + priorityWeight + sep + deltaV + sep + numTimeSlots);


        }
        else {
            System.out.println(experimentID + sep + n + sep + seed + sep + ef + sep + s + sep + last + sep + avg + sep + max + sep +
                    last2 + sep + avg2 + sep + max2 + sep + agentsValue + sep + searchedCSCount + sep + iteration + sep + timeLimit + sep + statisticsTimer.getTime() + sep + gain
                    + sep + stabilityRatio[0] + sep + stabilityRatio[1] + sep + deviationStrategy + sep + solutionSelection + sep + alphaSolSel + sep + maxAlpha + sep + bestFitness);
        }
    }


    public ArrayList<String> getCoalitionStructuresList() {
        return coalitionStructuresList;
    }

    public ArrayList<Double> getCoalitionsValueTimeSeries() {
        return coalitionsValueTimeSeries;
    }

    public void startTimer() {
        statisticsTimer.start();
    }
}
