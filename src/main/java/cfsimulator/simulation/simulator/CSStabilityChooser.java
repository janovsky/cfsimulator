package cfsimulator.simulation.simulator;

import org.apache.log4j.Logger;

import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.StabilityChecker;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * This class can be used to calculate stability of coalitions and coalition structures.
 * It can also pick the best coalition structure based on social wlfare and stability.
 *
 * Created by janovsky on 2/5/16.
 */
public class CSStabilityChooser {
    private static final boolean VERBOSE = ConfigurationProvider.getInstance().getBooleanProperty("simulation.CSStabilityChooser.verbose");
    private AgentStorage agentStorage;
    private ArrayList<CoalitionStructure> candidates;
    private ArrayList<HashSet<String>> toSkipList;
    private Logger logger = Logger.getLogger(this.getClass());

    public CSStabilityChooser(AgentStorage agentStorage) {
        this.agentStorage = agentStorage;
    }

    public CoalitionStructure chooseBestSolution(ArrayList<String> coalitionStructuresList, int maxAlpha) {
        candidates = generateCandidateSolutions(coalitionStructuresList);
        toSkipList = new ArrayList<>();
        candidates.forEach(c -> {
            toSkipList.add(new HashSet<>());
            c.setAlphaStableList(new ArrayList<>());
            c.getAlphaStableList().add(1.0);
        });

        int alphaTop = computeStability(maxAlpha);

        return pickSolution(alphaTop);

    }

    private CoalitionStructure pickSolution(int alphaTop) {
        double weightValue = ConfigurationProvider.getInstance().getDoubleProperty("simulation.stability.CSValueWeight");
        double weightStability = ConfigurationProvider.getInstance().getDoubleProperty("simulation.stability.stabilityWeight");

        double maxFitness = -Double.MAX_VALUE;
        CoalitionStructure bestCandidate = null;

        ArrayList<Double> reshapedCSValues = reshapeCSValues();

        for (int i = 0; i < candidates.size(); i++) {
            CoalitionStructure cs = candidates.get(i);
            double alphaStability = cs.getAlphaStability(alphaTop);
            double fitness = weightValue * reshapedCSValues.get(i) + weightStability * alphaStability;
            cs.setFitness(fitness);
            if (fitness > maxFitness) {
                maxFitness = fitness;
                bestCandidate = cs;
            }
        }
        return bestCandidate;
    }

    private ArrayList<Double> reshapeCSValues() {
        ArrayList<Double> reshapedCSValues = new ArrayList<>(candidates.size());
        double maxV = -Double.MAX_VALUE;
        double minV = Double.MAX_VALUE;
        for (CoalitionStructure cs : candidates) {
            double v = cs.getValue();
            if (v > maxV) maxV = v;
            if (v < minV) minV = v;
        }

        for (CoalitionStructure cs : candidates) {
            //shift minimum to 0, rescale so that maximum is 1
            reshapedCSValues.add((cs.getValue() - minV) / (maxV - minV));
        }
        return reshapedCSValues;
    }

    private int computeStability(int maxAlpha) {
        if (maxAlpha <= 0) maxAlpha = Integer.MAX_VALUE;
        int alphaTop = 0;
        for (int alpha = 1; alpha <= Math.min(agentStorage.getNumberOfAgents(), maxAlpha); alpha++) {
            boolean allComputed = true;

            for (int i = 0; i < candidates.size(); i++) {
                CoalitionStructure cs = candidates.get(i);
                if (cs.isStabilityComputationComplete()) continue;
                alphaStable(alpha, cs, toSkipList.get(i));
                if (!cs.isStabilityComputationComplete()) allComputed = false;
            }
            if (allComputed) {
                alphaTop = alpha;
                if (VERBOSE) logger.info(alpha + "-stability calculated, finishing");
                break;
            }
            if (VERBOSE) logger.info(alpha + "-stability calculated");
            alphaTop = alpha;
        }
        return alphaTop;
    }

    private void alphaStable(int alpha, CoalitionStructure cs, HashSet<String> toSkip) {
        StabilityChecker.Result r = StabilityChecker.alphaStable(alpha, cs, toSkip, agentStorage);
        cs.setStabilityComputationComplete(!r.unresolved);
        cs.getAlphaStableList().add(((double) r.result) / cs.getNumberOfOpenCoalitions());
    }


    private ArrayList<CoalitionStructure> generateCandidateSolutions(ArrayList<String> coalitionStructuresList) {
        ArrayList<CoalitionStructure> ret = new ArrayList<>();
        coalitionStructuresList.forEach(csString -> {
            CoalitionStructure cs = new CoalitionStructure(csString, agentStorage);
            ret.add(cs);
        });
        return ret;
    }
}
