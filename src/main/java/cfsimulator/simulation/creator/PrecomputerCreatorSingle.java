package cfsimulator.simulation.creator;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import cfsimulator.simulation.simulator.Simulator;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.DatasetProvider;
import cfsimulator.utils.RandomProvider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Creator that given a file of problem instances creates evaluations of all possible coalitions for all problem instances.
 * Note that this single-threaded approach is faster than the solution using parallel (see PrecomputerCreator)
 *
 * Created by janovsky on 3/30/15.
 */
public class PrecomputerCreatorSingle {

    private static Simulator simulator;

    public static void main(String[] args) {


        List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
        loggers.add(LogManager.getRootLogger());
        for (Logger logger : loggers) {
            logger.setLevel(Level.OFF);
        }

        try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
            String line = br.readLine();

            while (line != null && !line.isEmpty()) {
                processLine(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void processLine(String line) {
        ArrayList<String> argList = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(line);
        while (st.hasMoreTokens()) {
            String s = st.nextToken();
            argList.add(s);
        }
        init(argList.toArray(new String[argList.size()]));
    }


    private static void init(String[] args) {
        //init configuration provider
        ConfigurationProvider.getInstance().init("configuration.properties", args);

        ConfigurationProvider.getInstance().setValue("evaluator.parseEvaluations", "" + false);
        ConfigurationProvider.getInstance().setValue("evaluator.evaluateAllCombinations", "" + true);

        String ef = ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction");
        ef = ef.substring(ef.lastIndexOf(".") + 1);
        int seed = ConfigurationProvider.getInstance().getIntProperty("simulation.randomSeed");
        int n = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfAgents");
        File file = new File("dpidpinput/" + ef + "-n" + n + "-s" + seed);
        if (!file.exists()) {
            System.out.println(file.getName());
            //init random provider
            RandomProvider.getInstance().init(ConfigurationProvider.getInstance().getIntProperty("simulation.randomSeed"));

            //load energy load profiles
            if ((ConfigurationProvider.getInstance().getBooleanProperty("simulation.evaluation.EnergyPurchasingEvaluationFunction.realData")
                    && ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("simulation.evaluation.EnergyPurchasingEvaluationFunction")) ||
                    (ConfigurationProvider.getInstance().getBooleanProperty("simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction.realData")
                            && ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction"))){
                DatasetProvider.getInstance().init();
            }


            //init simulator
            simulator = new Simulator();
            try {
                simulator.init();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
