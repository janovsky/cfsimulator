package cfsimulator.simulation.creator;

import java.io.*;
import java.util.ArrayList;

/**
 * Generator of problem instances. One line is generated for each problem instance.
 * All lines are written to a file which is then used as the input file for running EXPERIMENTS.
 *
 * Created by janovsky on 3/4/15.
 */
public class InstancesGenerator {

    private static final int maxSeed = 10;
    private static final int maxNumberOfAgents = 20;
    private static String outputFileName = "experiments/data.in";
    private static boolean compareWithDP = false;

    private static StringBuilder sb = new StringBuilder();
    private static String[] evaluationFunctions = new String[]{
//            "cfsimulator.simulation.evaluation.MarketBasedEvaluationFunction",
//            "cfsimulator.simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction",
//            "cfsimulator.simulation.evaluation.DistanceToTargetEvaluationFunction",
//            "cfsimulator.simulation.evaluation.DistanceToFlatEvaluationFunction",
//            "cfsimulator.simulation.evaluation.BoundedSumEvaluationFunction",
//            "cfsimulator.simulation.evaluation.NormalDistributionEvaluationFunction",
//            "cfsimulator.simulation.evaluation.UniformDistributionEvaluationFunction",
//            "cfsimulator.simulation.evaluation.NDCSEvaluationFunction",
//            "cfsimulator.simulation.evaluation.EnergyPurchasingEvaluationFunction",
            "cfsimulator.simulation.evaluation.RenewableEnergyEvaluationFunction",
            "cfsimulator.simulation.evaluation.PrioritizedRenewableEnergyEvaluationFunction"

    };
    private static String[] strategies = new String[]{
            /*"cfsimulator.simulation.strategy.CoalitionThresholdValueBasedStrategy",*/
//            "cfsimulator.simulation.strategy.CoalitionValueBasedStrategy",
//            "cfsimulator.simulation.strategy.StayIfWinStrategy",
//            "cfsimulator.simulation.strategy.CommunityDetectionBasedStrategy",
//            "cfsimulator.simulation.strategy.CommunityDetectionEFBasedStrategy",
//            "cfsimulator.simulation.strategy.RandomStrategy",
            "cfsimulator.simulation.strategy.LocalSearchStrategy",
//            "cfsimulator.simulation.strategy.MixedStrategy"
    };

    private static String[] mixedWeights = new String[]{
            "1;1",
//            "1;2",
//            "2;1"
    };

    private static String profitSharing = "EntryMarginalContribution";
//    private static String profitSharing = "EqualSharing";

    private static boolean simulationStabilityAllAgentsMustProfit = false;



    //    private static int[] agentCounts = new int[]{200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000, 2200, 2400, 2600, 2800, 3000};
//    private static int[] agentCounts = new int[]{20, 100, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000};
//    private static int[] agentCounts = new int[]{100, 200, 300, 400, 500, 600, 700, 800, 900, 1000};
    private static int[] agentCounts = new int[]{69};
//    private static int[] agentCounts = new int[]{20,30,40,50,60,70,80,90,100};
    //    private static int[] agentCounts = new int[]{20, 30, 40, 50, 60, 70, 80, 90, 100};
//    private static int[] agentCounts = new int[]{1000};
    //    private static int[] agentCounts = new int[]{5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500, 10000};
//    private static int[] timeLimits = new int[]{60000, 120000, 180000, 240000, 300000};
    private static int[] timeLimits = new int[]{600000, 900000, 1200000, 1800000, 2400000, 3000000, 3600000};
    private static ArrayList<String> mixedStrategies = new ArrayList<>();
    //    private static Boolean[] deviate = new Boolean[]{true, false};

    private static Boolean[] simulationStabilityChooserON = new Boolean[]{false};
    private static Boolean[] deviate = new Boolean[]{false};
    private static boolean stabilityCheckerON = false;

    private static int gammaOffset = 10;
//    private static boolean simulationStabilityChooserON = true;

    private static int[] maxAlphaSolutionSelection = new int[]{3};

    private static int distanceToFlatInterestVectorSize = 3;
    private static int basicInterestVectorSize = 10;
    private static int experimentCounter = 0;
    private static boolean timeLimitEnabled = false;
    private static long timeLimit = 5000;
    private static int[] numberOfIterations = new int[]{1000};
    private static boolean newLine = true;

//    private static double[] uncertaintyCoverage = new double [] {0, 0.2, 0.4, 0.6, 0.8, 1.0};
    private static double[] uncertaintyCoverage = new double [] {0.4};
    private static double[] commitmentWithoutCF = new double [] {0.8};
//    private static double[] commitmentWithoutCF = new double [] {0.7};
//    private static int[] numberOfCoalitionLeaders = new int[]{5, 7, 10, 13, 15, 18, 20, 25, 30, 35, 40};
//    private static int[] numberOfCoalitionLeaders = new int[]{10};
    private static int[] numberOfCoalitionLeaders = new int[]{5};
    private static int[] numberOfEnergyStores = new int[]{5, 10, 15, 20, 25, 30, 35, 40};
//    private static int[] numberOfEnergyStores = new int[]{20};

    // following two are bound together
//    private static boolean[] distributeRemainingESPower = new boolean[]{false, true, true};
//    private static boolean[] EScanObserveRealGeneration = new boolean[]{false, false, true};
    private static boolean[] distributeRemainingESPower = new boolean[]{false};
    private static boolean[] EScanObserveRealGeneration = new boolean[]{false};

    private static boolean[] useRemainingRGPower = new boolean[]{false};
//    private static boolean[] useRemainingRGPower = new boolean[]{false, true};

//    private static double[] priorityWeight = new double[]{0.001, 0.0011, 0.0012, 0.0013, 0.0014, 0.0015, 0.0016, 0.0017, 0.0018, 0.0019, 0.002};
//    private static double[] priorityWeight = new double[]{0.001, 0.01, 0.1, 1};
//    private static double[] priorityWeight = new double[]{1};
private static double[] priorityWeight = new double[]{0.0075};

//    private static int[] numberOfTimeSlots = new int[]{1, 2, 4, 6, 8, 10};
private static int[] numberOfTimeSlots = new int[]{1, 2, 5, 10, 15, 20, 24};
//    private static int[] numberOfTimeSlots = new int[]{1, 2, 4, 6, 8, 10, 12, 14, 16, 15, 20, 22, 24};
//    private static int[] numberOfTimeSlots = new int[]{1};
//private static int[] numberOfTimeSlots = new int[]{24};

    public static void main(String[] args) {

        initMixedStrategies();
        createInstances();
        System.out.println("Number of instances created: " + experimentCounter);
    }

    private static void initMixedStrategies() {
        for (int i = 0; i < strategies.length; i++) {
            String s1 = strategies[i];
            if (!s1.equals("cfsimulator.simulation.strategy.MixedStrategy") && !s1.equals("cfsimulator.simulation.strategy.LocalSearchStrategy")) {
                for (int j = i; j < strategies.length; j++) {
                    String s2 = strategies[j];
                    if (!s1.equals(s2) && !s2.equals("cfsimulator.simulation.strategy.MixedStrategy") && !s2.equals("cfsimulator.simulation.strategy.LocalSearchStrategy")) {
                        mixedStrategies.add(s1 + ";" + s2);
                    }
                }
            }
        }
    }

    private static void createInstances() {
        for (int seed = 1; seed <= maxSeed; seed++) {
//            for (int agentCounter = 2; agentCounter <= maxNumberOfAgents; agentCounter++) {
            for (int agentCounter : agentCounts) {
//        int agentCounter = 10000;
//      TIME LIMITS
//            for (long t : timeLimits) {
//                timeLimit = t;
                timeLimit = Math.max(5000, (agentCounter * 120));
//                timeLimit = (long) Math.max(5000, ((double) agentCounter / 100) * 60000);
                for (String ef : evaluationFunctions) {
                    for (String s : strategies) {
                        for (int d = 0; d < deviate.length; d++) {
                            for (int c = 0; c < simulationStabilityChooserON.length; c++) {
                                for (int iter = 0; iter < numberOfIterations.length; iter++) {
                                    for (double uc : uncertaintyCoverage) {
                                        for (double com : commitmentWithoutCF) {
                                            for (int maxAlphaSolSel : maxAlphaSolutionSelection) {
                                                for (int numCoalLeaders : numberOfCoalitionLeaders) {
                                                    for (int numES : numberOfEnergyStores) {
                                                        for(int esp = 0; esp < distributeRemainingESPower.length; esp ++)
                                                        {
                                                            for(boolean useRG : useRemainingRGPower) {
                                                                for (double priorWeight : priorityWeight) {
                                                                    for (int numTimeSlots : numberOfTimeSlots) {

                                                                        boolean distRemESPower = distributeRemainingESPower[esp];
                                                                        boolean EScanObserveReal = EScanObserveRealGeneration[esp];
                                                                        int mixedStrategyCounter = 0;
                                                                        if (s.equals("cfsimulator.simulation.strategy.MixedStrategy")) {
                                                                            for (int i = 0; i < mixedStrategies.size(); i++) {
                                                                                for (String w : mixedWeights) {
                                                                                    addLine(agentCounter, ef, s, seed, mixedStrategyCounter, w, deviate[d], simulationStabilityChooserON[c], numberOfIterations[iter], maxAlphaSolSel, uc, com,
                                                                                            numCoalLeaders, numES, distRemESPower, EScanObserveReal, useRG, priorWeight, numTimeSlots);
                                                                                }
                                                                                mixedStrategyCounter++;
                                                                            }
                                                                        } else {
                                                                            addLine(agentCounter, ef, s, seed, mixedStrategyCounter, null, deviate[d], simulationStabilityChooserON[c], numberOfIterations[iter], maxAlphaSolSel, uc, com,
                                                                                    numCoalLeaders, numES, distRemESPower, EScanObserveReal, useRG, priorWeight, numTimeSlots);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
//            }
            }
        }
        writeToFile(outputFileName);
        System.out.println("INSTANCES GENERATED");
    }

    private static void addLine(int agentCounter, String ef, String s, int seed, int mixedStrategyCounter, String mixedWeight,
                                Boolean deviate, Boolean stabilityChooser, int numberOfIterations,
                                int maxAlphaSolSel, double uncertaintyCoverage, double commitmentWithoutCF, int numCoalLeaders, int numES,
                                boolean distRemESPower, boolean EScanObserveReal, boolean useRG, double priorWeight, int numTimeSlots) {
        experimentCounter++;
        add("experimentID", "" + experimentCounter);
        add("simulation.evaluationFunction", ef);
        add("agent.strategy", s);
        add("evaluator.agent", "cfsimulator.simulation.evaluation.BasicEvaluatorAgent");
        add("simulation.randomSeed", "" + seed);
        add("simulation.numberOfAgents", "" + agentCounter);
        add("simulation.timeLimitEnabled", "" + timeLimitEnabled);
        if (timeLimitEnabled) {
            add("simulation.timeLimit", "" + timeLimit);
        } else {
            add("simulation.numberOfIterations", "" + numberOfIterations);
        }
        add("statistics.showGraphs", "" + false);
//        add("evaluator.evaluateAllCombinations", "" + true);
//        add("hierarchicalModularity.optimizingFunction", "evaluationFunction");
        switch (ef) {
            case "cfsimulator.simulation.evaluation.DistanceToFlatEvaluationFunction":
                add("agent.interestVectorSize", "" + distanceToFlatInterestVectorSize);
                add("simulation.evaluation.EnergyPurchasingEvaluationFunction.realData", "" + false);
                break;
            case "cfsimulator.simulation.evaluation.EnergyPurchasingEvaluationFunction":
                add("simulation.evaluation.EnergyPurchasingEvaluationFunction.realData", "" + true);
                add("simulation.evaluation.EnergyPurchasingEvaluationFunction.dataFile", "LD_2014_Jan_AVG.txt");
                add("evaluationFunction.gamma", "1.1");
                break;
            case "cfsimulator.simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction":
                add("simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction.realData", "" + true);
                add("simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction.dataFile", "IntTrade.txt");
                add("evaluationFunction.gamma", "2.0");

//                add("agent.interestVectorSize", "" + basicInterestVectorSize);
                break;
//            case "simulation.evaluation.RenewableEnergyEvaluationFunction":
//                add("renewableScenario.priorityOrder", "" + false);
//                break;
//            case "simulation.evaluation.PrioritizedRenewableEnergyEvaluationFunction":
//                add("renewableScenario.priorityOrder", "" + true);
//                break;
            default:
                add("agent.interestVectorSize", "" + basicInterestVectorSize);
                add("simulation.evaluation.EnergyPurchasingEvaluationFunction.realData", "" + false);
                break;
        }

//        if (s.equals("simulation.strategy.CommunityDetectionBasedStrategy") || s.equals("simulation.strategy.MixedStrategy")) {
//            add("CDrecommender.run", "true");
//        } else {
//            add("CDrecommender.run", "false");
//        }
        if (s.equals("cfsimulator.simulation.strategy.MixedStrategy")) {
            add("agent.strategy.mixed", mixedStrategies.get(mixedStrategyCounter));
        }
        if (mixedWeight != null) {
            add("agent.strategy.mixed.weights", mixedWeight);
        }
        add("evaluator.parseEvaluations", "" + compareWithDP);
        add("evaluator.evaluateAllCombinations", "" + false);
        add("agent.strategy.deviate", "" + deviate);
        add("simulation.CSStabilityChooser.ON", "" + stabilityChooser);
        add("profitSharing", profitSharing);
        add("agent.strategy.deviate.RandomOrder", "" + profitSharing.equals("EqualSharing"));
        add("simulation.stability.allAgentsMustProfit", "" + simulationStabilityAllAgentsMustProfit);
        add("simulation.CSStabilityChooser.maxAlpha", "" + maxAlphaSolSel);
        add("simulation.stabilityChecker.ON", "" + stabilityCheckerON);
        add("evaluationFunction.gammaOffset", "" + gammaOffset);
        add("renewableScenario.uncertaintyCoverage", "" + uncertaintyCoverage);
        add("renewableScenario.commitmentWithoutCF", "" + commitmentWithoutCF);
        add("simulation.numberOfCoalitionLeaders", "" + numCoalLeaders);
        add("renewableScenario.energyStoreCount", "" + numES);
        add("renewableScenario.distributeRemainingESPower", "" + distRemESPower);
        add("renewableScenario.EScanObserveRealGeneration", "" + EScanObserveReal);
        add("renewableScenario.useRemainingRGPower", "" + useRG);
        add("renewableScenario.priorityWeight", "" + priorWeight);
        add("renewableScenario.numberOfSlots", "" + numTimeSlots);

        newLine();
    }

    private static void writeToFile(String fileName) {
        Writer writer = null;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileName), "utf-8"));
            writer.write(sb.toString());
        } catch (IOException ex) {
            System.out.println("IO Exception");
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {
            }
        }
    }

    private static void newLine() {
        sb.append("\n");
        newLine = true;
    }

    private static void add(String argument, String value) {
        if(newLine){
            sb.append("-").append(argument).append(" ").append(value);
            newLine = false;

        }
        else{
            sb.append(" -").append(argument).append(" ").append(value);
        }
    }
}
