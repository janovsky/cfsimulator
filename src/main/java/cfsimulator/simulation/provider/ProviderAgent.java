package cfsimulator.simulation.provider;

import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.evaluation.EvaluatorAgent;

/**
 * Currently unused.
 * A central agent that operates with offers and goods.
 *
 * Created by janovsky on 2/18/15.
 */
public interface ProviderAgent extends EvaluatorAgent {
    ComodityTypes comodityType = null;

    InterestVector showOffers();

    void sell(InterestVector amount, Coalition coalition);

    void makeAssignments();

    void acceptOffer(InterestVector amount, Coalition coalition);

    void createOffers();


}
