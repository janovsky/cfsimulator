package cfsimulator.simulation.agent;

/**
 * Polarity of the InterestVector. Value depends on the problem being solved.
 *
 * Created by janovsky on 2/18/15.
 */
public enum InterestVectorPolarity {
    POSITIVE, NEGATIVE, ANY
}
