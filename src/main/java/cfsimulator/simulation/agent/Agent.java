package cfsimulator.simulation.agent;

import cfsimulator.renewableEnergy.EnergyEstimatesProvider;
import cfsimulator.renewableEnergy.IEEE69Bus;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionFactory;
import cfsimulator.simulation.simulator.AgentStorage;
import cfsimulator.simulation.strategy.MixedStrategy;
import cfsimulator.simulation.strategy.Strategy;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.DatasetProvider;
import cfsimulator.utils.RandomProvider;

import java.util.ArrayList;
import java.util.Random;

/**
 * Agent representation. The simulation loop calls the update method which makes the agent decide whether to leave a coalition
 * and which coalition to join based on the Strategy used.
 *
 * Created by janovsky on 2/10/15.
 */
public class Agent {
    private final AgentStorage agentStorage;
    private Strategy strategy;
    private InterestVector interestVector;
    private String name;
    private Coalition coalition;
    private int profit = 0;
    private int id;
    /**
     * utilityTypeID is used when different agents can have different utilities
     * i.e. can evaluate coalitions using different evaluation functions
     */
    private int utilityTypeID;

    private boolean participatesInCF;
    private boolean isCoalitionLeader = false;
    private boolean isEnergyStore = false;


    private IEEE69Bus.Node gridNode;


    public Agent(int id, AgentStorage agentStorage, boolean participatesInCF, boolean coalitionLeader, boolean energyStore) {
        this.name = "Agent" + id;
        this.id = id;
        this.agentStorage = agentStorage;
        this.participatesInCF = participatesInCF;
        this.isCoalitionLeader = coalitionLeader;
        this.isEnergyStore = energyStore;


        try {
            strategy = (Strategy) Class.forName(ConfigurationProvider.getInstance().getStringProperty("agent.strategy")).newInstance();

            if (strategy instanceof MixedStrategy) {
                ArrayList<String> strategiesString = ConfigurationProvider.getInstance().getListProperty("agent.strategy.mixed");
                ArrayList<String> strategiesWeightsString = ConfigurationProvider.getInstance().getListProperty("agent.strategy.mixed.weights");
                ArrayList<Strategy> strategies = new ArrayList<>();
                ArrayList<Double> weights = new ArrayList<>();
                for (int i = 0; i < strategiesString.size(); i++) {
                    String s = strategiesString.get(i);
                    strategies.add((Strategy) Class.forName(s).newInstance());
                    weights.add(Double.parseDouble(strategiesWeightsString.get(i)));
                }

                ((MixedStrategy) strategy).init(strategies, weights);
            }

        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Setup specific scenarios
        String scenario = ConfigurationProvider.getInstance().getStringProperty("scenario");
        if(scenario.equalsIgnoreCase("renewablescenario"))
        {
            if(ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.IEEE69Grid"))
            {
                // RENEWABLE SCENARIO WITH THE UNDERLYING PHYSICAL CONSTRAINTS REPRESENTED BY THE IEEE 69 GRID
                int numberOfSlots = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots");
                double scale = ConfigurationProvider.getInstance().getDoubleProperty("renewableScenario.scale");
                Random r = RandomProvider.getInstance().getRandom();

                if (isCoalitionLeader) {
                    // RENEWABLE GENERATOR
                    interestVector = new InterestVector(EnergyEstimatesProvider.getInstance().getRequestedCoverage(this));
                }
                else if(isEnergyStore){
                    //ENERGY STORE
                    ArrayList<Double> list = new ArrayList<>();

                    if(ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.flatGeneration"))
                    {
                        // THE REASON FOR THIS IS TO TRY WHETHER MAKING THE CF PROBLEM EASIER IMPROVES THE PRIORITIZED VALUATION FUNCTION APPROACH
                        // add total amount of storage
                        list.add(10 * scale * 1.0);
                        // add start
                        int start = r.nextInt(numberOfSlots);//0;
                        list.add((double) start);
                        //add end
                        int end = start + r.nextInt(numberOfSlots - start);//23;
                        list.add((double) end);
                    }
                    else {
                        // add total amount of storage
//                        list.add(0.5 * numberOfSlots * scale * r.nextDouble());
                        list.add(0.25 * numberOfSlots * scale * r.nextDouble());
                        // add start
                        int start = r.nextInt(numberOfSlots);
                        list.add((double) start);
                        //add end
                        int end = start + r.nextInt(numberOfSlots - start);
                        list.add((double) end);
                    }

                    interestVector = new InterestVector(list);
                }
                else
                {
                    // LOAD
                    // Initialize load with negative values from U(-scale, 0)

                    ArrayList<Double> list = new ArrayList<>();
                    for (int i = 0; i < numberOfSlots; i++) {
                        list.add( -1 * scale * r.nextDouble());
                    }
                    interestVector = new InterestVector(list);
                }
            }
            else {
                // EUMAS PAPER: RENEWABLE SCENARIO WITHOUT THE UNDERLYING POWER GRID
                int numberOfSlots = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots");
                double scale = 100;
                Random r = RandomProvider.getInstance().getRandom();

                if (isCoalitionLeader) {
                    interestVector = new InterestVector(EnergyEstimatesProvider.getInstance().getRequestedCoverage(this));
                } else {
                    this.isEnergyStore = true;
                    ArrayList<Double> list = new ArrayList<>();
                    // add total amount of storage
                    list.add(scale * r.nextDouble());
                    // add start
                    int start = r.nextInt(numberOfSlots);
                    list.add((double) start);
                    //add end
                    int end = start + r.nextInt(numberOfSlots - start);
                    list.add((double) end);
                    interestVector = new InterestVector(list);
                }
            }
        }
        else {
            InterestVectorPolarity polarity = InterestVectorPolarity.ANY;
            InterestVectorDiscretization discretization = InterestVectorDiscretization.CONTINUOUS;
            if (ConfigurationProvider.getInstance().getStringProperty("evaluator.agent").equals("simulation.provider.TaskProviderAgent") ||
                    ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.BoundedSumEvaluationFunction") ||
                    ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.EnergyPurchasingEvaluationFunction") ||
                    ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.DistanceToTargetEvaluationFunction")) {
                polarity = InterestVectorPolarity.POSITIVE;
            }
            if (ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.DistanceToFlatEvaluationFunction")) {
                polarity = InterestVectorPolarity.POSITIVE;
                discretization = InterestVectorDiscretization.DISCRETE;
            }
            if ((ConfigurationProvider.getInstance().getBooleanProperty("simulation.evaluation.EnergyPurchasingEvaluationFunction.realData")
                    && ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.EnergyPurchasingEvaluationFunction")) ||
                    ((ConfigurationProvider.getInstance().getBooleanProperty("simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction.realData")
                            && ConfigurationProvider.getInstance().getStringProperty("simulation.evaluationFunction").equals("cfsimulator.simulation.evaluation.MarketBasedSizePenalizedEvaluationFunction")))) {
                interestVector = DatasetProvider.getInstance().createInterestVector();
            } else {
                interestVector = new InterestVector(ConfigurationProvider.getInstance().getIntProperty("agent.interestVectorSize"), polarity, discretization);
            }
        }
    }

    public void update() {
        if(!participatesInCF)
        {
            // THIS AGENT DOES NOT PARTICIPATE IN COALITION FORMATION.
            return;
        }

        if(isCoalitionLeader)
        {
            // coalition leaders don't leave the coalition
        }
        else {
            if (strategy.leaveCoalition(coalition, agentStorage.getCoalitionStructure(), this)) {
                if(coalition != null) coalition.removeAgent(name);
                coalition = strategy.pickCoalition(agentStorage.getCoalitionStructure(), this);
                if(coalition != null) {
                    coalition.addAgent(this);
                    if (coalition.isNew()) {
                        agentStorage.getCoalitionStructure().addCoalition(coalition);
                    }
                }
            }
        }
    }

    /**
     * figure out whether the agent wants to deviate, potentially deviate
     *
     * @return true iff the agent deviated
     */
    public boolean deviate() {
        ArrayList<String> deviateWith = strategy.deviate(coalition, this);
        boolean deviate = (deviateWith != null);
        //if agent wants to deviate
        if (deviate) {
            coalition.removeAgents(deviateWith);
            coalition = CoalitionFactory.getInstance().createCoalition(deviateWith, agentStorage);
            coalition.notifyAgents();
            agentStorage.getCoalitionStructure().addCoalition(coalition);
        }
        return deviate;
    }

    public String getState() {
        return "[ " + name + ": " + interestVector.toString() + "]";
    }

    public String getName() {
        return name;
    }

    public InterestVector getInterestVector() {
        return interestVector;
    }

    public AgentStorage getAgentStorage() {
        return agentStorage;
    }

    public void assignProfit(int profit) {

        this.profit += profit;
        coalition.increaseAggregateProfit(profit);
    }

    public int getProfit() {
        return profit;
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public int getId() {
        return id;
    }

    public Coalition getCoalition() {
        return coalition;
    }

    public void setCoalition(Coalition coalition) {
        this.coalition = coalition;
    }

    public String toString() {
        return name;
    }


    public int getUtilityTypeID() {
        return utilityTypeID;
    }

    public void setUtilityTypeID(int utilityTypeID) {
        this.utilityTypeID = utilityTypeID;
    }

    public boolean isCoalitionLeader() {
        return isCoalitionLeader;
    }

    public void setCoalitionLeader(boolean b)
    {
        isCoalitionLeader = b;
    }

    public IEEE69Bus.Node getGridNode() {
        return gridNode;
    }

    public void setGridNode(IEEE69Bus.Node gridNode) {

        this.gridNode = gridNode;
        gridNode.agent = this;
    }

    public boolean isEnergyStore() {
        return isEnergyStore;
    }

    public boolean participatesInCF()
    {
        return participatesInCF;
    }
}
