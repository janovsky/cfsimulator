package cfsimulator.simulation.coalition;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.agent.InterestVector;
import cfsimulator.simulation.evaluation.EvaluatorAgent;
import cfsimulator.simulation.provider.Customer;
import cfsimulator.simulation.provider.ProviderAgent;
import cfsimulator.simulation.simulator.AgentStorage;
import cfsimulator.utils.ConfigurationProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * Coalition representation. Holds the map of agents in the coalition, their aggregate interest vector
 * and the value of the coalition computed by the given Evaluation Function
 * Coalition can ONLY be created using the CoalitionFactory, which will assign the new coalition the correct ID.
 *
 * Created by janovsky on 2/10/15.
 */
public class Coalition implements Customer, Comparable {
    private static boolean UPDATE_CURRENT_MARGINALS = true;
    protected final AgentStorage agentStorage;
    private final String name;
    private final EvaluatorAgent evaluatorAgent;
    protected ArrayList<String> agentsOrder = new ArrayList<>();
    /**
     * marginal contribution of each agent at the time it joined this coalition.
     */
    protected HashMap<String, Double> marginalContributions = new HashMap<>();

    private int aggregateProfit;
    private HashMap<String, Agent> agentsMap;
    private Double value = 0.0;



    private boolean isClosed = false;
    private InterestVector aggregateInterestVector;
    /**
     * set to false when the coalition is added to the coalition structure
     */
    private boolean isNew = true;
    /**
     * current marginal contribution of each agent
     */
    private HashMap<String, Double> currentMarginalContributions = new HashMap<>();
    private double totalCurrentMarginalContribution;
    protected Agent leader;


    protected Coalition(String name, Agent a, AgentStorage agentStorage) {
        this.name = name;
        this.agentStorage = agentStorage;
        this.evaluatorAgent = agentStorage.getEvaluatorAgent();

        if(a.isCoalitionLeader())
        {
            leader = a;
        }

        agentsMap = new HashMap<>();
        marginalContributions = new HashMap<>();

        agentsMap.put(a.getName(), a);
        agentsOrder.add(a.getName());
        aggregateInterestVector = new InterestVector(a.getInterestVector());
        aggregateProfit = a.getProfit();
        updateValue();
        marginalContributions.put(a.getName(), value);

    }

    protected Coalition(String name, ArrayList<String> agentNames, AgentStorage agentStorage) {
        this.name = name;
        this.agentStorage = agentStorage;
        this.evaluatorAgent = agentStorage.getEvaluatorAgent();

        agentsMap = new HashMap<>();
        boolean coalitionLeaders = ConfigurationProvider.getInstance().getBooleanProperty("simulation.coalitionLeaders");

        //add first agent
        Agent first = null;
        if (!agentNames.isEmpty()) {
            //coalition leader has to be the first agent
            first = coalitionLeaders ? findLeader(agentStorage, agentNames) : agentStorage.getAgent(agentNames.get(0));
            if(coalitionLeaders)
                init(first);
            agentsMap.put(first.getName(), first);
            agentsOrder.add(first.getName());
            aggregateInterestVector = new InterestVector(first.getInterestVector());
            aggregateProfit = first.getProfit();

            updateValue();
            marginalContributions.put(first.getName(), value);
        }

        for (String agentName : agentNames) {
            Agent a = agentStorage.getAgent(agentName);
            if (a != first)
                addAgent(a, false);
        }
        updateValue();
    }

    public void init(Agent first) {
        // Used by RenewableEnergyCoalition
    }

    public HashMap<String, Agent> getAgentsMap() {
        return agentsMap;
    }

    public String getState() {
        StringBuilder sb = new StringBuilder();
        sb.append("[ ").append(name).append(": value: ").append(value).append(", size: ").append(getSize()).append("; ").append("IV: ").append(aggregateInterestVector.toString()).append("; ");
        agentsMap.forEach((k, v) -> sb.append(v.getName()).append(","));
        sb.append("]");
        return sb.toString();
    }

    public void removeAgent(String agentName) {
        removeAgent(agentName, true);
    }

    /**
     * remove agents without updating the value of this coalition
     * NOTE: creates temporary inconsistent state
     *
     * @param agentName name of the agent
     */
    public void removeAgentLazy(String agentName) {
        removeAgent(agentName, false);
    }

    public void removeAgent(String agentName, boolean update) {
        Agent a = agentsMap.get(agentName);
        // only update aggregate interest vector in case of homogeneous interest vectors
        // Example of non-homogeneous IV is renewable energy scenario
        if(aggregateInterestVector.getSize() == a.getInterestVector().getSize()) {
            aggregateInterestVector = aggregateInterestVector.subtract(a.getInterestVector());
        }
        aggregateProfit -= a.getProfit();
        agentsMap.remove(agentName);
        agentsOrder.remove(agentName);
        marginalContributions.remove(agentName);
        if (agentsMap.isEmpty()) {
            isClosed = true;
            agentStorage.addCoalitionToRemove(name);
            agentStorage.getCoalitionStructure().closeCoalition(name);
        }
        if (update) {
            updateValue();
            updateEntryMarginalContributions();
        }
    }

    public void removeAgents(ArrayList<String> agentsToRemove) {
        agentsToRemove.forEach(this::removeAgentLazy);
        updateEntryMarginalContributions();
        updateValue();
    }

    protected void updateValue() {
        value = evaluatorAgent.evaluateSingle(this);
        if ((ConfigurationProvider.getInstance().getBooleanProperty("agent.strategy.deviate") || ConfigurationProvider.getInstance().getBooleanProperty("simulation.stabilityChecker.ON"))
                && UPDATE_CURRENT_MARGINALS
                && ConfigurationProvider.getInstance().getStringProperty("profitSharing").equals(ProfitSharing.CurrentMarginalContribution.toString())) {
            updateCurrentMarginalContributions();
        }
    }

    protected void updateEntryMarginalContributions() {
        double value = 0;
        ArrayList<String> temp = new ArrayList<>();
        for (String agent : agentsOrder) {
            temp.add(agent);
            Coalition c = CoalitionFactory.getInstance().createCoalition(temp, agentStorage);
            marginalContributions.put(agent, c.getCoalitionValue() - value);
            value = c.getCoalitionValue();
        }
    }

    /**
     * compute current marginal contributions of all agents by removing them temporarily
     */
    private void updateCurrentMarginalContributions() {
        if (agentsOrder.size() == 1) {
            currentMarginalContributions.put(agentsOrder.get(0), 0.0);
            return;
        }
        // this flag prevents temporary sub-coalitions from evaluating further sub-coalition recursively
        UPDATE_CURRENT_MARGINALS = false;
        totalCurrentMarginalContribution = 0;
        for (int i = 0; i < agentsOrder.size(); i++) {
            String curAgent = agentsOrder.remove(i);
            Coalition temp = CoalitionFactory.getInstance().createCoalition(agentsOrder, agentStorage);
            double v = value - temp.getCoalitionValue();
            currentMarginalContributions.put(curAgent, v);
            totalCurrentMarginalContribution += v;
            agentsOrder.add(i, curAgent);
        }
        UPDATE_CURRENT_MARGINALS = true;
    }

    public String getName() {
        return name;
    }

    public void addAgent(Agent agent) {
        addAgent(agent, true);
    }


    public void addAgent(Agent agent, boolean updateValue) {

        if (!agentsMap.containsKey(agent.getName())) {
            agentsMap.put(agent.getName(), agent);
            agentsOrder.add(agent.getName());

            // only update aggregate interest vector in case of homogeneous interest vectors
            // Example of non-homogeneous IV is renewable energy scenario
            if(aggregateInterestVector.getSize() == agent.getInterestVector().getSize())
            {
                aggregateInterestVector = aggregateInterestVector.add(agent.getInterestVector());
            }
            aggregateProfit += agent.getProfit();
            double oldValue = value;
            updateValue();
            marginalContributions.put(agent.getName(), value - oldValue);
        }
    }

    public Double getCoalitionValue() {
        return value;
    }

    /**
     * Since askingAgent does not influence the coalition value, the getCoalitionValue() method is called.
     * This is different in the HeterogeneousCoalition, where the coalition value is taken with respect to the askingAgent
     *
     * @param askingAgent agent that is asking for the value
     * @return coalition value
     */
    public Double getCoalitionValue(Agent askingAgent) {
        return getCoalitionValue();
    }

    public Integer getSize() {
        return agentsMap.size();
    }

    public Iterator<Agent> getAgentsIterator() {
        return agentsMap.values().iterator();
    }

    /**
     * @return list of agents' names in the order they joined the coalition
     */
    public ArrayList<String> getAgentsOrdered() {
        return agentsOrder;
    }

    /**
     * computes the new value by adding the agent, computing the value and removing the agent again
     *
     * @param agent agent that is considered for addition
     * @return coalition value with the agent
     */
    public Double getValueWithAgent(Agent agent) {
        boolean alreadyIn = (agentsMap.containsKey(agent.getName()));
        if (alreadyIn) {
            return getCoalitionValue(agent);
        } else {
            addAgent(agent);
            double potentialValue = getCoalitionValue(agent);
            removeAgent(agent.getName());
            return potentialValue;
        }
    }

    /**
     * computes the new value by adding the agent, computing the value and removing the agent again
     *
     * @param agent agent that is considered for addition
     * @return marginal contribution with the agent
     */
    public Double getMarginalContributionWithAgent(Agent agent) {
        boolean alreadyIn = (agentsMap.containsKey(agent.getName()));
        if (alreadyIn) {
            return getEntryMarginalContribution(agent.getName());
        } else {
            addAgent(agent);
            double potentialValue = getEntryMarginalContribution(agent.getName());
            removeAgent(agent.getName());
            return potentialValue;
        }
    }

    public InterestVector getAggregateInterestVector() {
        return aggregateInterestVector;
    }

    @Override
    public void sendOffer(ProviderAgent providerAgent) {
        providerAgent.acceptOffer(aggregateInterestVector, this);
    }

    public void increaseAggregateProfit(int profit) {
        aggregateProfit += profit;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Object o) {
        Coalition other = (Coalition) o;
        if (value > other.getCoalitionValue()) {
            return -1;
        } else {
            return 1;
        }
    }

    public boolean contains(String agentName) {
        return agentsMap.containsKey(agentName);
    }

    public EvaluatorAgent getEvaluatorAgent() {
        return evaluatorAgent;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public String toAgentString() {
        StringBuilder sb = new StringBuilder();
        Iterator<Agent> agentsIterator = getAgentsIterator();
        sb.append("[ ");
        while (agentsIterator.hasNext()) {
            sb.append(agentsIterator.next().getName()).append(" ");
        }
        sb.append("]");
        return sb.toString();
    }

    public String toNumericallyOrderedString() {
        Iterator<Agent> agentsIterator = getAgentsIterator();
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        while (agentsIterator.hasNext()) {
            queue.add(agentsIterator.next().getId());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        while (!queue.isEmpty()) {
            sb.append(queue.poll());
            if (!queue.isEmpty()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    public String toEntryOrderedString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (int i = 0; i < agentsOrder.size(); i++) {
            sb.append(agentsOrder.get(i));
            if (i < agentsOrder.size() - 1) sb.append(",");
        }
        sb.append("}");
        return sb.toString();
    }

    /**
     * @param agentName agent's name
     * @return agent's profit given the specified profit sharing rule
     */
    public double getProfit(String agentName) {
        switch (ConfigurationProvider.getInstance().getStringProperty("profitSharing")) {
            case "EntryMarginalContribution":
                return getEntryMarginalContribution(agentName);
            case "CurrentMarginalContribution":
                return getFairProfit(agentName);
            case "EqualSharing":
                return getEqualSharingProfit(agentName);
        }
        return -1;
    }

    /**
     * @param agentName agent's name
     * @return marginal contribution that the agent had when entering this coalition
     */
    public double getEntryMarginalContribution(String agentName) {
        return marginalContributions.get(agentName);
    }


    /**
     *
     * @param agentName agent's name
     * @return marginal contribution that the agent has now
     */
    public double getCurrentMarginalContribution(String agentName) {
        return currentMarginalContributions.get(agentName);
    }

    /**
     * @param agentName agent's name
     * @return profit of the agent as coalition value divided by number of agents in the coalition
     */
    public double getEqualSharingProfit(String agentName) {
        if (!agentsMap.containsKey(agentName)) return 0;
        return value / agentsMap.size();
    }

    /**
     * @param agentName agent's name
     * @return profit of this agent based on marginal contribution that the agent has now
     */
    public double getFairProfit(String agentName) {
        if (totalCurrentMarginalContribution == 0) return 0;
        return value * (currentMarginalContributions.get(agentName) / totalCurrentMarginalContribution);
    }

    /**
     * notify agents that this is their new coalition
     * This is necessary when agents change coalitions by other agents decisions.
     */
    public void notifyAgents() {
        Iterator<Agent> agentsIterator = getAgentsIterator();
        agentsIterator.forEachRemaining(a -> a.setCoalition(this));
    }

    public Agent getLeader() {
        return leader;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public int getAggregateProfit() {
        return aggregateProfit;
    }

    public Agent findLeader(AgentStorage agentStorage, ArrayList<String> agentNames) {
        for(String name : agentNames)
        {
            Agent a = agentStorage.getAgent(name);
            if(a.isCoalitionLeader())
                return a;
        }
        //we didn't find any coalition leader. This should never happen.
        return null;
    }

    public enum ProfitSharing {
        EntryMarginalContribution, CurrentMarginalContribution
    }

}
