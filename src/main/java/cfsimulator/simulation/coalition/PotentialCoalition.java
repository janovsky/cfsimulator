package cfsimulator.simulation.coalition;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.utils.ConfigurationProvider;

/**
 * A helper wrapper class used by the CoalitionValueBasedStrategy to sort the potential new coalitions of the given Agent
 *
 * Created by janovsky on 2/16/15.
 */
public class PotentialCoalition implements Comparable {
    @Deprecated
    private Double potentialValue;

    private final double deltaValue;
    private Coalition coalition;

    public PotentialCoalition(Coalition coalition, Agent agent) {
        boolean localEvaluationOnly = ConfigurationProvider.getInstance().getBooleanProperty("evaluator.localEvaluationOnly");

        if(!localEvaluationOnly) {
            if (coalition.contains(agent.getName())) {
                deltaValue = coalition.getEntryMarginalContribution(agent.getName());
                potentialValue = coalition.getCoalitionValue(agent);
            } else {
                double oldValue = coalition.getCoalitionValue(agent);
                potentialValue = coalition.getValueWithAgent(agent);
                deltaValue = potentialValue - oldValue;
            }
        }
        else
        {
            // in this case the agents only care about themselves, no one cares about the coalition value
            // we will therefore sort based on profit for the given agent
            // NOTE: WE DON'T SUPPORT POTENTIAL VALUE ANYMORE IN THIS CASE
            deltaValue = coalition.getMarginalContributionWithAgent(agent);
        }
        this.coalition = coalition;
    }

    @Override
    public int compareTo(Object o) {
        PotentialCoalition other = (PotentialCoalition) o;
        if (other.getDeltaValue() > deltaValue) {
            return 1;
        } else {
            return -1;
        }
    }

    @Deprecated
    public Double getPotentialValue() {
        return potentialValue;
    }

    public Coalition getCoalition() {
        return coalition;
    }

    public double getDeltaValue() {
        return deltaValue;
    }
}
