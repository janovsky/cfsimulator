package cfsimulator.simulation.coalition;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.evaluation.EvaluatorAgent;
import cfsimulator.simulation.simulator.AgentStorage;
import cfsimulator.utils.ConfigurationProvider;

import java.util.ArrayList;

/**
 * Factory that MUST be used when creating a new Coalition.
 * The correct coalition name is assigned to the new Coalition.
 *
 * Created by janovsky on 2/13/15.
 */
public class CoalitionFactory {
    private static CoalitionFactory instance = null;
    private final String coalitionsName = "Coalition";
    private int coalitionCounter = 0;

    protected CoalitionFactory() {
        // Exists only to defeat instantiation.
    }

    public static CoalitionFactory getInstance() {
        if (instance == null) {
            instance = new CoalitionFactory();
        }
        return instance;
    }

    public Coalition createCoalition(Agent a) {
        return createCoalition(a, a.getAgentStorage());
    }

    public Coalition createCoalition(Agent a, AgentStorage agentStorage) {
        String scenario = ConfigurationProvider.getInstance().getStringProperty("scenario");
        if(scenario.equalsIgnoreCase("renewablescenario")){
            return new RenewableEnergyCoalition(coalitionsName + (coalitionCounter++), a, agentStorage);
        }
        else if (!ConfigurationProvider.getInstance().getBooleanProperty("evaluator.heterogeneousEvaluation")) {
            return createHomogeneousCoalition(a, agentStorage);
        } else {
            return createHeterogeneousCoalition(a, agentStorage, agentStorage.getEvaluatorAgentList());
        }
    }

    public Coalition createCoalition(ArrayList<String> agentNames, AgentStorage agentStorage) {
        String scenario = ConfigurationProvider.getInstance().getStringProperty("scenario");
        if(scenario.equalsIgnoreCase("renewablescenario")){
            return new RenewableEnergyCoalition(coalitionsName + (coalitionCounter++), agentNames, agentStorage);
        }
        if (!ConfigurationProvider.getInstance().getBooleanProperty("evaluator.heterogeneousEvaluation")) {
            return createHomogeneousCoalition(agentNames, agentStorage);
        } else {
            return createHeterogeneousCoalition(agentNames, agentStorage, agentStorage.getEvaluatorAgentList());
        }
    }

    private Coalition createHomogeneousCoalition(Agent a, AgentStorage agentStorage) {
        return new Coalition(coalitionsName + (coalitionCounter++), a, agentStorage);
    }

    private Coalition createHomogeneousCoalition(ArrayList<String> agentNames, AgentStorage agentStorage) {
        return new Coalition(coalitionsName + (coalitionCounter++), agentNames, agentStorage);
    }

    private HeterogeneousCoalition createHeterogeneousCoalition(Agent a, AgentStorage agentStorage, ArrayList<EvaluatorAgent> evaluatorAgentList) {
        return new HeterogeneousCoalition(coalitionsName + (coalitionCounter++), a, agentStorage, evaluatorAgentList);
    }

    private HeterogeneousCoalition createHeterogeneousCoalition(ArrayList<String> agentNames, AgentStorage agentStorage, ArrayList<EvaluatorAgent> evaluatorAgentList) {
        return new HeterogeneousCoalition(coalitionsName + (coalitionCounter++), agentNames, agentStorage, evaluatorAgentList);
    }

    public void reset() {
        coalitionCounter = 0;
    }
}
