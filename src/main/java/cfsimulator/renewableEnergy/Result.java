package cfsimulator.renewableEnergy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.utils.ConfigurationProvider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class stores the result of coalition structure evaluation in the renewable energy domain.
 *
 * Created by janovsky on 7/27/16.
 */
public class Result {

    public CoalitionStructure coalitionStructure;
    public double profit = 0;
    public double profitWithoutCF = 0;
    public double renewableGeneration = 0;
    public double renewableGenerationWithoutCF = 0;
    public double generationProfit = 0;
    public double uncertaintyCost = 0;
    public double failedToProvideCost = 0;
    public double generationProfitWithoutCF = 0;
    public double failedToProvideWithoutCFCost = 0;
    public double committedUsedEnergy = 0;
    public int numberOfPosViolations = 0;
    public int numberOfNegViolations = 0;
    public int numberOfPosViolationsWithoutCF;
    public int numberOfNegViolationsWithoutCF;
    public Map<Integer, double[]> voltageMap;
    public Map<Agent, List<Double>> loads;
    public int[] numberOfPosViolationsVector = new int[ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots")];
    public int[] numberOfNegViolationsVector = new int[ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots")];
    public double deltaV = 0;

    public Result(double profit, double profitWithoutCF, double renewableGeneration, double renewableGenerationWithoutCF) {
        this.profit = profit;
        this.profitWithoutCF = profitWithoutCF;
        this.renewableGeneration = renewableGeneration;
        this.renewableGenerationWithoutCF = renewableGenerationWithoutCF;
    }

    public Result(CoalitionStructure cs) {
        this.coalitionStructure = cs;
    }

    public void add(Result result) {
        profit += result.profit;
        profitWithoutCF += result.profitWithoutCF;
        renewableGeneration += result.renewableGeneration;
        renewableGenerationWithoutCF += result.renewableGenerationWithoutCF;
        generationProfit += result.generationProfit;
        uncertaintyCost += result.uncertaintyCost;
        failedToProvideCost += result.failedToProvideCost;
        generationProfitWithoutCF += result.generationProfitWithoutCF;
        failedToProvideWithoutCFCost += result.failedToProvideWithoutCFCost;
        committedUsedEnergy += result.committedUsedEnergy;
    }

    public void addVoltages(int slot, double[] v) {
        if (voltageMap == null) {
            voltageMap = new HashMap<>(24);
        }
        voltageMap.put(slot, v);
    }
}
