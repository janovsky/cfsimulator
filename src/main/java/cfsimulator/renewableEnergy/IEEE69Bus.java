package cfsimulator.renewableEnergy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.simulator.AgentStorage;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.RandomProvider;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * IEEE 69 Bus with hard-coded nodes and edges.
 * The bus was presented in the following paper:
 * Khatod, Pant, Sharma: Novel approach for sensitivity calculations in the radial distribution system, 2006
 *
 * Created by janovsky on 1/2/17.
 */
public class IEEE69Bus
{

    private static final int N = 69;


    private List<Set<Node>> adjacencyList;
    private Map<Integer, Node> nodeMap;
    private double[][] impedanceR = new double[N][N];
    private double[][] impedanceI = new double[N][N];

    private void initGrid()
    {
        adjacencyList = new ArrayList<>(N);
        nodeMap = new HashMap<>(N);
        for (int i = 0; i < N; i++)
        {
            adjacencyList.add(new LinkedHashSet<>());
        }

        // data without impedance
        /*
        //middle branch
        add(1,2);
        add(2,3);
        add(3,4);
        add(4,5);
        add(5,6);
        add(6,7);
        add(7,8);
        add(8,9);
        add(9,10);
        add(10,11);
        add(11,12);
        add(12,13);
        add(13,14);
        add(14,15);
        add(15,16);
        add(16,17);
        add(17,18);
        add(18,19);
        add(19,20);
        add(20,21);
        add(21,22);
        add(22,23);
        add(23,24);
        add(24,25);
        add(25,26);
        add(26,27);

        //top right branch
        add(3,28);
        add(28,29);
        add(29,30);
        add(30,31);
        add(31,32);
        add(32,33);
        add(33,34);
        add(34,35);

        //top left branch
        add(4,36);
        add(36,37);
        add(37,38);
        add(38,39);

        //center right branch
        add(8,40);
        add(40,41);

        //bottom left branch
        add(9,42);
        add(42,43);
        add(43,44);
        add(44,45);
        add(45,46);
        add(46,47);
        add(47,48);
        add(48,49);
        add(49,50);
        add(50,51);
        add(51,52);
        add(52,53);
        add(53,54);

        //center left branch 1
        add(11,55);
        add(55,56);

        //center left branch 2
        add(12,57);
        add(57,58);

        //center right branch 1
        add(3,59);
        add(59,60);
        add(60,61);
        add(61,62);
        add(62,63);
        add(63,64);
        add(64,65);
        add(65,66);
        add(66,67);
        add(67,68);
        add(68,69);
        */

        // data with impedance
        //middle branch
        add(1, 2, 0.000311963, 0.00074871);
        add(2, 3, 0.000311963, 0.00074871);
        add(3, 4, 0.000935888, 0.002246131);
        add(4, 5, 0.015660525, 0.01834340);
        add(5, 6, 0.228356656, 0.116299674);
        add(6, 7, 0.237777928, 0.121103899);
        add(7, 8, 0.057525912, 0.029324489);
        add(8, 9, 0.030759517, 0.015660525);
        add(9, 10, 0.510994811, 0.168896576);
        add(10, 11, 0.116798814, 0.038620975);
        add(11, 12, 0.44386045, 0.146684835);
        add(12, 13, 0.642643047, 0.212134598);
        add(13, 14, 0.651378001, 0.215254225);
        add(14, 15, 0.660112955, 0.218124281);
        add(15, 16, 0.122663712, 0.040555144);
        add(16, 17, 0.233597628, 0.077241951);
        add(17, 18, 0.002932449, 0.00099828);
        add(18, 19, 0.204397925, 0.067571109);
        add(19, 20, 0.131398666, 0.0434252);
        add(20, 21, 0.213132879, 0.070441165);
        add(21, 22, 0.008734954, 0.002870056);
        add(22, 23, 0.099266513, 0.03281847);
        add(23, 24, 0.216065327, 0.071439446);
        add(24, 25, 0.467195256, 0.154421509);
        add(25, 26, 0.192730522, 0.063702772);
        add(26, 27, 0.10806386, 0.035688527);

        //top right branch
        add(3, 28, 0.002745271, 0.006738393);
        add(28, 29, 0.039931218, 0.097644308);
        add(29, 30, 0.24819748, 0.082046175);
        add(30, 31, 0.043799555, 0.014475067);
        add(31, 32, 0.218997776, 0.072375333);
        add(32, 33, 0.523473317, 0.175697361);
        add(33, 34, 1.065664393, 0.352268218);
        add(34, 35, 0.919665876, 0.304038793);

        //top left branch
        add(4, 36, 0.002121346, 0.005240972);
        add(36, 37, 0.053096042, 0.129963638);
        add(37, 38, 0.180813549, 0.442425422);
        add(38, 39, 0.051286659, 0.125471376);

        //center right branch
        add(8, 40, 0.057900267, 0.029511666);
        add(40, 41, 0.207080803, 0.069505277);

        //bottom left branch
        add(9, 42, 0.108563, 0.055279781);
        add(42, 43, 0.126656834, 0.064513875);
        add(43, 44, 0.177319567, 0.090281989);
        add(44, 45, 0.175510184, 0.089408494);
        add(45, 46, 0.992041209, 0.332988927);
        add(46, 47, 0.488970249, 0.164092351);
        add(47, 48, 0.189798073, 0.062766884);
        add(48, 49, 0.240897554, 0.073124044);
        add(49, 50, 0.316642084, 0.161284687);
        add(50, 51, 0.060770323, 0.030946694);
        add(51, 52, 0.090469167, 0.046045686);
        add(52, 53, 0.443298918, 0.225798562);
        add(53, 54, 0.649506226, 0.330805188);

        //center left branch 1
        add(11, 55, 0.125533768, 0.038121835);
        add(55, 56, 0.002932449, 0.000873495);

        //center left branch 2
        add(12, 57, 0.461330358, 0.152487341);
        add(57, 58, 0.002932449, 0.00099828);

        //center right branch 1
        add(3, 59, 0.002745271, 0.006738393);
        add(59, 60, 0.039931218, 0.097644308);
        add(60, 61, 0.065699333, 0.076742811);
        add(61, 62, 0.018967329, 0.022149348);
        add(62, 63, 0.001123066, 0.001310243);
        add(63, 64, 0.454404788, 0.530898028);
        add(64, 65, 0.193416839, 0.226048132);
        add(65, 66, 0.025580937, 0.029823629);
        add(66, 67, 0.005740113, 0.007237533);
        add(67, 68, 0.067945464, 0.085664942);
        add(68, 69, 0.000561533, 0.00074871);
    }

    /**
     * initialize the IEEE 69 bus system.
     * @param agentMap: has to be of size N
     */
    public void init(Map<String, Agent> agentMap)
    {
        assert(agentMap.size() == N);
        initGrid();

        ArrayList<String> agentNames = new ArrayList<>(agentMap.keySet());

        // sort by name to enforce determinism
        agentNames.sort(String::compareTo);

        for (int i = 1; i <= N; i++) {
            String a = agentNames.get(RandomProvider.getInstance().getAgentStorageRandom().nextInt(agentNames.size()));
            agentNames.remove(a);
            Agent agent = agentMap.get(a);

            Node node = nodeMap.get(i);
            if(agent.isCoalitionLeader())
            {
                node.type = Node.Type.RENEWABLE_GENERATOR;
            }
            else if(agent.isEnergyStore())
            {
                node.type = Node.Type.ENERGY_STORE;
            }
            agent.setGridNode(nodeMap.get(i));
//            System.out.println("Node " + node.id + " -> " + node.type);
        }
    }

    /**
     * Add edge between parent and child nodes.
     *
     * @param nodeID1 parent node
     * @param nodeID2 child node
     * @param impR    real impedance
     * @param impI    imaginary impedance
     */
    private void add(int nodeID1, int nodeID2, double impR, double impI) {
        Node n1 = nodeMap.computeIfAbsent(nodeID1, Node::new);
        Node n2 = nodeMap.computeIfAbsent(nodeID2, Node::new);
        n2.parent = n1;
        getAdjSet(nodeID1).add(n2);
        getAdjSet(nodeID2).add(n1);

        //add impedance
        impedanceR[nodeID1 - 1][nodeID2 - 1] = impR;
        impedanceI[nodeID1 - 1][nodeID2 - 1] = impI;
    }

    private Set<Node> getAdjSet(int nodeID)
    {
        return adjacencyList.get(nodeID - 1);
    }

    /**
     *
     * @param generation list of generations.
     *                   Negative generation represents load
     * @param result    object that stores among other things the evaluation result
     */
    public void evaluate(Map<Agent, List<Double>> generation, Result result) {
        LoadFlow.loadFlowDay(generation, impedanceR, impedanceI, result);
    }

    public Set<Node> getBranchNeighbors(Node n, int heightDiff, boolean includeSelf) {
        Set<Node> ret = new LinkedHashSet<>();
        if (includeSelf)
            ret.add(n);

        ret.addAll(getAncestors(n, heightDiff));
        ret.addAll(getDescendants(n, heightDiff));

        return ret;
    }

    /**
     * Finds descendants of the node in the grid tree structure, NOT INCLUDING n
     *
     * @param n
     * @param heightDiff
     * @return
     */
    public Set<Node> getDescendants(Node n, int heightDiff) {
        // no need for a close list since the grid is a tree
        LinkedList<Node> open = new LinkedList<>();
        Map<Node, Integer> heightMap = new HashMap<>();

        Set<Node> ret = new LinkedHashSet<>();

        open.add(n);
        heightMap.put(n, 0);

        while (!open.isEmpty()) {
            Node curNode = open.poll();
            ret.add(curNode);
            int curHeight = heightMap.get(curNode);
            if (curHeight == heightDiff) {
                // we reached the height, do not expand this node
                continue;
            }
            Set<Node> adjSet = getAdjSet(curNode.id);
            for (Node child : adjSet) {
                if (curNode.parent != null && curNode.parent.id == child.id) {
                    // this is the parent, so skip the node
                    continue;
                }
                open.add(child);
                // the child has a higher height than the parent
                heightMap.put(child, curHeight + 1);
            }
        }

        ret.remove(n);
        return ret;

    }

    /**
     * Finds ancestors of the node in the grid tree structure, NOT INCLUDING n
     *
     * @param n
     * @param heightDiff
     * @return
     */
    public Set<Node> getAncestors(Node n, int heightDiff) {
        Set<Node> ret = new LinkedHashSet<>();
        Node curNode = n;
        for (int i = 0; i < heightDiff; i++) {
            if (curNode.parent == null) {
                // we found the root
                break;
            } else {
                curNode = curNode.parent;
                ret.add(curNode);
            }
        }
        return ret;
    }

    public Map<Integer, Node> getNodeMap() {
        return nodeMap;
    }

    public List<Set<Node>> getAdjacencyList() {
        return Collections.unmodifiableList(adjacencyList);
    }

    public void findPriorities(AgentStorage agentStorage) {

        Map<Integer, double[]> voltageMap = agentStorage.initialVoltageMap;
        Iterator<Agent> agentsIterator = agentStorage.getAgentsIterator();

        int neighborhoodSize = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.neighborhoodSize");

        while (agentsIterator.hasNext()) {
            Agent a = agentsIterator.next();
            if (a.isCoalitionLeader() || a.isEnergyStore()) {
                Set<Node> branchNeighbors = getBranchNeighbors(a.getGridNode(), neighborhoodSize, true);

                double upVotes = 0;
                double downVotes = 0;

                // loop over the time slots
                for (Integer slot : voltageMap.keySet()) {
                    double[] voltage = voltageMap.get(slot);
                    double branchUpVotes = 0;
                    double branchDownVotes = 0;
                    // loop over the branch neighbors
                    for (Node n : branchNeighbors) {

                        double v = voltage[n.id - 1];
                        if (v < 0.95) {
                            // this note needs higher voltage
                            branchUpVotes += 1;
                            upVotes += 1;
                        } else if (v > 1.05) {
                            // this node needs lower voltage
                            branchDownVotes += 1;
                            downVotes += 1;
                        }
                    }

                    a.getGridNode().upVotesVector[slot] = branchUpVotes;
                    a.getGridNode().downVotesVector[slot] = branchDownVotes;
                }

                a.getGridNode().upVotes = upVotes;
                a.getGridNode().downVotes = downVotes;
            }
        }
    }

    public static class Node
    {

        public final int id;
        public Type type = Type.LOAD;

        /**
         * Voltage level, used only for display
         */
        public double voltage = 0;
        /**
         * Load level, used only for display
         */
        public double load;

        public Agent agent;
        public Node parent;
        public double upVotes = 0;
        public double downVotes = 0;

        /**
         * Energy committed by energy stores, used only for display
         */
        public double committed = 0;
        /**
         * Energy actually generated by energy stores, used only for display
         */
        public double used = 0;

        public double[] upVotesVector = new double[ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots")];
        public double[] downVotesVector = new double[ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots")];

        public Node(int id) {

            this.id = id;
        }

        public String toString() {

            BigDecimal bd = new BigDecimal(voltage);
            bd = bd.setScale(4, RoundingMode.HALF_UP);

            // display in kW
            BigDecimal bd2 = new BigDecimal(load / 1000);
            bd2 = bd2.setScale(2, RoundingMode.HALF_UP);

            BigDecimal bd3 = new BigDecimal(committed);
            bd3 = bd3.setScale(1, RoundingMode.HALF_UP);

            BigDecimal bd4 = new BigDecimal(used);
            bd4 = bd4.setScale(1, RoundingMode.HALF_UP);

            String ret = type.toString() + ":" + id + ", " + agent.getName() + "\n" + "V:" + bd.doubleValue() + "\nP:" + bd2.doubleValue();
            if (type.equals(Type.ENERGY_STORE) || type.equals(Type.RENEWABLE_GENERATOR)) {
                ret = ret + "\n+:" + upVotes + " -:" + downVotes;
            }
            if (type.equals(Type.ENERGY_STORE)) {
                ret = ret + "\ncom:" + bd3.doubleValue() + "%\nused:" + bd4.doubleValue() + "%";
            }
            return ret;
        }

        public double getPriority() {
            return upVotes - downVotes;
        }


        public enum Type {
            LOAD("L"), RENEWABLE_GENERATOR("RG"), ENERGY_STORE("ES");

            private String string;

            Type(String name) {
                string = name;
            }

            @Override
            public String toString() {
                return string;
            }

        }

    }

}
