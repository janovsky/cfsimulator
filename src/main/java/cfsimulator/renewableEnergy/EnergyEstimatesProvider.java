package cfsimulator.renewableEnergy;

import cfsimulator.simulation.agent.Agent;
import cfsimulator.utils.ConfigurationProvider;
import cfsimulator.utils.RandomProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * This class provides various energy load vectors.
 *
 * Created by janovsky on 7/21/16.
 */
public class EnergyEstimatesProvider {
    private static EnergyEstimatesProvider ourInstance = new EnergyEstimatesProvider();
    int numberOfSlots = ConfigurationProvider.getInstance().getIntProperty("renewableScenario.numberOfSlots");
    double uncertaintyCoverage = ConfigurationProvider.getInstance().getDoubleProperty("renewableScenario.uncertaintyCoverage");
    double estimateDelta = ConfigurationProvider.getInstance().getDoubleProperty("renewableScenario.estimateDelta");
    double scale = ConfigurationProvider.getInstance().getDoubleProperty("renewableScenario.scale");
    private HashMap<Agent, ArrayList<Double>> estimates = new HashMap<>();
    private HashMap<Agent, ArrayList<Double>> realValues = new HashMap<>();
    private EnergyEstimatesProvider() {
    }

    public static EnergyEstimatesProvider getInstance() {
        return ourInstance;
    }

    public ArrayList<Double> getRequestedCoverage(Agent agent)
    {
        ArrayList<Double> requested = new ArrayList<>(getEstimate(agent));

        // apply uncertainty coverage
        for(int i = 0; i < requested.size(); i ++)
        {
            requested.set(i, requested.get(i) * uncertaintyCoverage);
        }
        return requested;
    }


    public ArrayList<Double> getEstimate(Agent agent)
    {
        // first compute real values
        ArrayList<Double> realVals = getRealValues(agent);

        // then compute deviation from the real values based on Normal distribution and return it
        return new ArrayList<>(estimates.computeIfAbsent(agent, (a) ->
        {
            Random r = RandomProvider.getInstance().getRandom();

            ArrayList<Double> list = new ArrayList<>();
            for (int i = 0; i < numberOfSlots; i++) {
                double d = ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.flatGeneration")
                        ? 0.5 : r.nextGaussian();
                double est = Math.max(realVals.get(i) + scale * estimateDelta * d, 0);

                list.add(est);
            }
            return list;
        }));
    }

    public ArrayList<Double> getRealValues(Agent agent)
    {
        return realValues.computeIfAbsent(agent, (a) ->
        {
            Random r = RandomProvider.getInstance().getRandom();

            ArrayList<Double> list = new ArrayList<>();
            for (int i = 0; i < numberOfSlots; i++) {
                double d = ConfigurationProvider.getInstance().getBooleanProperty("renewableScenario.flatGeneration")
                        ? 1.0 : r.nextDouble();
                list.add(scale * d);
            }
            return list;
        });
    }




}
