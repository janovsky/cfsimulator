package cfsimulator.renewableEnergy;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;
import cfsimulator.simulation.agent.Agent;
import cfsimulator.simulation.coalition.Coalition;
import cfsimulator.simulation.coalition.CoalitionStructure;
import cfsimulator.simulation.coalition.RenewableEnergyCoalition;
import cfsimulator.utils.ConfigurationProvider;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;
import java.util.List;

/**
 *
 * This class can be used to display the IEEE 69-bus test system
 *
 * Created by janovsky on 1/26/17.
 */
public class IEEE69Graph extends JFrame {


    public IEEE69Graph(IEEE69Bus grid, Result result, int slot, String name) {
        super(name);

        HashMap<Agent, Color> colors = generateColors(grid.getNodeMap());
        CoalitionStructure cs = result.coalitionStructure;
        mxGraph graph = new mxGraph();
        Object parent = graph.getDefaultParent();
        Map<Agent, List<Double>> loads = result.loads;

        mxStylesheet stylesheet = graph.getStylesheet();
        Hashtable<String, Object> recStyle = new Hashtable<>();
        recStyle.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE);
        recStyle.put(mxConstants.STYLE_OPACITY, 50);
        recStyle.put(mxConstants.STYLE_FONTCOLOR, "#000000");
        recStyle.put(mxConstants.STYLE_STROKEWIDTH, 10);
        stylesheet.putCellStyle("RECTANGLE", recStyle);

        Hashtable<String, Object> roundStyle = new Hashtable<>();
        roundStyle.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_ELLIPSE);
        roundStyle.put(mxConstants.STYLE_OPACITY, 50);
        roundStyle.put(mxConstants.STYLE_FONTCOLOR, "#000000");
        stylesheet.putCellStyle("ROUND", roundStyle);

        Hashtable<String, Object> hexagonStyle = new Hashtable<>();
        hexagonStyle.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_HEXAGON);
        hexagonStyle.put(mxConstants.STYLE_OPACITY, 50);
        hexagonStyle.put(mxConstants.STYLE_FONTCOLOR, "#000000");
        hexagonStyle.put(mxConstants.STYLE_STROKEWIDTH, 10);
        stylesheet.putCellStyle("HEXAGON", hexagonStyle);

        graph.getModel().beginUpdate();

        try {
            List<Set<IEEE69Bus.Node>> adjacencyList = grid.getAdjacencyList();
            Map<Integer, IEEE69Bus.Node> nodeMap = grid.getNodeMap();
            double[] voltageVector = result.voltageMap.get(slot);
            Map<IEEE69Bus.Node, Object> cellMap = new HashMap<>();


            for (Integer i : nodeMap.keySet()) {
                IEEE69Bus.Node node = nodeMap.get(i);
                double voltage = voltageVector[node.id - 1];
                node.voltage = voltage;
                double load = loads.get(node.agent).get(slot);
                node.load = load;

                Agent coalitionLeader = null;
                Coalition coal = cs.getCoalitionFor(node.agent);
                if (coal != null) {
                    coalitionLeader = coal.getLeader();
                }

                Color c = coalitionLeader != null ? colors.get(coalitionLeader) : Color.WHITE;
                String hex = String.format("#%02X%02X%02X", c.getRed(), c.getGreen(), c.getBlue());

                if(node.agent.isEnergyStore() && coal != null)
                {
                    double committed = ((RenewableEnergyCoalition)coal).getTotalCommittedByStore(node.agent);
                    double used = ((RenewableEnergyCoalition)coal).getTotalUsedByStore(node.agent);
                    double available = node.agent.getInterestVector().get(0);
                    node.committed = 100 * committed / available;
                    node.used = 100 * used / available;
                }

                String style;
                switch (node.type) {
                    case RENEWABLE_GENERATOR:
                        style = "RECTANGLE";
                        break;
                    case ENERGY_STORE:
                        style = "HEXAGON";
                        break;
                    case LOAD:
                    default:
                        style = "ROUND";
                        break;
                }

                style = style + ";strokeColor=" + hex + ";";
                if (voltage < 0.95) {
                    style = style + "fillColor=blue";
                } else if (voltage > 1.05) {
                    style = style + "fillColor=red";
                } else {
                    style = style + "fillColor=green";
                }
                Object o = graph.insertVertex(parent, "" + node.id, node, i * 20, 20, 50, 50, style);
                cellMap.put(node, o);
            }

            for (Integer i : nodeMap.keySet()) {
                IEEE69Bus.Node node = nodeMap.get(i);
                Set<IEEE69Bus.Node> adjSet = adjacencyList.get(node.id - 1);
                for (IEEE69Bus.Node other : adjSet) {
                    graph.insertEdge(parent, null, null, cellMap.get(node), cellMap.get(other));
                }
            }

        } finally {
            graph.getModel().endUpdate();
        }

        // define layout
        mxHierarchicalLayout layout;

        boolean printForThesis = false;
        if(printForThesis)
        {
            layout = new mxHierarchicalLayout(graph);
            layout.setInterRankCellSpacing(35);
        }
        else
        {
            layout = new mxHierarchicalLayout(graph, SwingConstants.WEST);
        }

//        layout.setOrientation(3);


        // layout graph
        layout.execute(graph.getDefaultParent());

        mxGraphComponent graphComponent = new mxGraphComponent(graph);
        getContentPane().add(graphComponent);


    }

    /**
     * http://stackoverflow.com/a/3403970
     * http://stackoverflow.com/questions/3403826/how-to-dynamically-compute-a-list-of-colors
     *
     * @param n
     * @return
     */
    public static Color[] getDifferentColors(int n) {
        Color[] cols = new Color[n];
        for (int i = 0; i < n; i++)
            cols[i] = Color.getHSBColor((float) i / n, 1, 1);
        return cols;
    }

    private HashMap<Agent, Color> generateColors(Map<Integer, IEEE69Bus.Node> nodeMap) {
        int numCoalitions = ConfigurationProvider.getInstance().getIntProperty("simulation.numberOfCoalitionLeaders");

        ArrayList<Agent> coalitionLeaders = new ArrayList<>(numCoalitions);
        for (IEEE69Bus.Node n : nodeMap.values()) {
            if (n.agent.isCoalitionLeader()) {
                coalitionLeaders.add(n.agent);
            }
        }
        Iterator<Agent> iterator = coalitionLeaders.iterator();

        HashMap<Agent, Color> map = new HashMap<>();
        Color[] colors = getDifferentColors(numCoalitions);
        for (int i = 0; i < numCoalitions; i++) {
            map.put(iterator.next(), colors[i]);
        }
        return map;
    }
}
