#!/bin/bash

-l mem=2G
-l h_rt=24:00:00
-pe single 16
-cwd
-j y
-N dataBigJob
./run_experiments_beocat.sh
-m abe
-M janovsky.paja@gmail.com

