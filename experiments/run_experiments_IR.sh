#!/bin/bash
instanceset=$1
cpus="24"
datain="data.in"
dataout="data.out"
dataouthead="data.out.head"
head="headIR.in"
mem="2"

# Send email notification
python sendemail.py -m "Experiments started"

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j CFSimulator_jarWithDependencies/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead


# Send email notification
python sendemail.py -m "ALL experiments completed"