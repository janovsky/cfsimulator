#!/bin/bash
head="head.opt"
dataouthead="optimal_solutions.head"

function generateOptimalSolutions() {
printf "" > "${2}"
for file in $(ls "$1")
do
arr=(${file//-/ })
ef="${arr[0]}"
n="${arr[1]:1}"
s="${arr[2]:1}"
printf "%s " "${n}" >> "${2}"
printf "%s " "${s}" >> "${2}"
printf "%s " "${ef}" >> "${2}"
#use this if you want to see the solution CS
#./DPIDP -n"${n}" -r"${1}/${file}" -s >> "${2}"
./DPIDP -n"${n}" -r"${1}/${file}" >> "${2}"
done

./addhead.sh $head $2 $dataouthead
}

generateOptimalSolutions dpidpinput optimal_solutions