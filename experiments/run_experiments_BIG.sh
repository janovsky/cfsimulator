#!/bin/bash
instanceset=$1
cpus="20"
datain="data20to3000.in"
dataout="data20to3000.out"
dataouthead="data20to3000.out.head"
head="head.in"
mem="2"

echo "Jobs to run: "
wc -l $datain

# Send email notification
python sendemail.py -m "Starting experiments"

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

# Send email notification
python sendemail.py -m "Experiment 1 completed"



datain="data3500to5000.in"
dataout="data3500to5000.out"
dataouthead="data3500to5000.out.head"

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

# Send email notification
python sendemail.py -m "Experiment 2 completed"

datain="data5500to6000.in"
dataout="data5500to6000.out"
dataouthead="data5500to6000.out.head"

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

# Send email notification
python sendemail.py -m "Experiment 3 completed"


datain="data6500to7000.in"
dataout="data6500to7000.out"
dataouthead="data6500to7000.out.head"

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

# Send email notification
python sendemail.py -m "Experiment 4 completed"

# Send email notification
python sendemail.py -m "ALL experiments completed"