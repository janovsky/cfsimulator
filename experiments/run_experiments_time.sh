#!/bin/bash
instanceset=$1
cpus="24"
##############################################
datain="dataTimeBig.in"
dataout="dataTimeBig.out"
dataouthead="dataTimeBig.out.head"
head="head.in"
mem="2"

echo "Jobs to run: "
wc -l $datain

echo "Running experiments"
./parallel_experiments.sh -j artifact/CFSimulator.jar -c $datain -o $dataout -m $mem"g" -v -s $cpus/:

./addhead.sh $head $dataout $dataouthead

#echo "Generating optimal solutions"

#./generateOptimalSolutions.sh