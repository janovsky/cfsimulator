# ## THIS SCRIPT GENERATES AN X-Y SCATTER PLOT FOR Stability vs Gain COMPARISON OF A SINGLE SIMULATION
# 
# fileName <- "stabilityScatter/stabilityVsGainDataNODEV.txt"
# 
# scatter <- read.csv(fileName, head=TRUE, sep=",")
# 
# pdf('stabilityScatter/stabilityVsGainScatterNODEV.pdf', width=5, height=5)
# 
# plot(scatter$CSGain, scatter$CSStability, main="Gain vs Stability", 
#      xlab="Gain", ylab="Stability", pch=19)
# 
# grid()
# dev.off() 
# 
# 
# 
# 
# #### Two sources
# fileName1 <- "stabilityScatter/stabilityVsGainData.txt"
# fileName2 <- "stabilityScatter/stabilityVsGainDataNODEV.txt"
# 
# 
# scatter1 <- read.csv(fileName1, head=TRUE, sep=",")
# scatter2 <- read.csv(fileName2, head=TRUE, sep=",")
# 
# pdf('stabilityScatter/stabilityVsGainScatterCOMPARE.pdf', width=5, height=5)
# 
# plot(scatter1$CSGain, scatter1$CSStability, main="Gain vs Stability", 
#      xlab="Gain", ylab="Stability", pch=4, col='red', xlim=c(0,max(scatter1$CSGain,scatter2$CSGain)), ylim=c(min(scatter1$CSStability, scatter2$CSStability),1))
# points(scatter2$CSGain, scatter2$CSStability, main="Gain vs Stability", 
#        xlab="Gain", ylab="Stability", pch=1,col='blue',xlim=c(0,max(scatter1$CSGain,scatter2$CSGain)), ylim=c(min(scatter1$CSStability, scatter2$CSStability),1))
# grid()
# legend('bottomleft',c('Deviation','No deviation'),pch=c(4,1),col=c('red','blue'))
# dev.off() 
# 
# 
# 
# 




#### Combinations
folder <- "scatter3"
showParetoOptimal <- FALSE


file.names <- dir(path=paste(getwd(),"/",folder,"/",sep=""),pattern=".nodev")

efs = character()
strategies = character()
devScatters = new.env(hash=T, parent=emptyenv())
nodevScatters = new.env(hash=T, parent=emptyenv())


for(i in 1:length(file.names)){
  nodevFile <- file.names[i]
  fileName <- substr(nodevFile,0,nchar(nodevFile) - 6) #remove file extension
  devFile <- paste(fileName, '.dev',sep="")
  nodevScatter <- read.table(paste("./",folder,"/",nodevFile,sep=""),header=TRUE, sep=",", stringsAsFactors=FALSE)
  devScatter <- read.table(paste("./",folder,"/",devFile,sep=""),header=TRUE, sep=",", stringsAsFactors=FALSE)
  tokens <- strsplit(fileName,'-',fixed=TRUE)[[1]]
  ef <- tokens[2]
  s <- tokens[3]
  efs = c(efs, ef)
  strategies = c(strategies, s)
  
  pdf(paste(folder,'/stabilityVsGainScatter-',ef,'-',s,'.pdf',sep=""), width=5, height=5)
  
  minY <- min(0.8, min(devScatter$CSStability,  nodevScatter$CSStability))
plot(1, type="n", xlab="Gain", ylab=expression("Stability"[alpha]), xlim=c(0,max(devScatter$CSGain,nodevScatter$CSGain)), ylim=c(minY,1))
  points(nodevScatter$CSGain, nodevScatter$CSStability, main="Gain vs Stability", 
         xlab="Gain", ylab="Stability", pch=1,col='blue')
  
  points(devScatter$CSGain, devScatter$CSStability, main="Gain vs Stability", 
       xlab="Gain", ylab="Stability", pch=4, col='red')
  
  devScatter$flag <- "ds"
  nodevScatter$flag <- "nods"
  d = rbind(devScatter, nodevScatter)
  D = d[order(d$CSGain,d$CSStability,decreasing=TRUE),]
  front = D[which(!duplicated(cummax(D$CSStability))),]
  frontDS = front[front$flag=="ds",]
  frontNODS = front[front$flag=="nods",]

if(showParetoOptimal){
  points(frontNODS$CSGain, frontNODS$CSStability, main="", 
         xlab="Gain", ylab="Stability", pch=1, cex=1, col='darkblue')
  points(frontDS$CSGain, frontDS$CSStability, main="", 
         xlab="Gain", ylab="Stability", pch=4, cex=1, col='darkred')
}
  grid()
  legend('bottomleft',c('Deviation','No deviation'),pch=c(4,1),col=c('red','blue'))
  dev.off() 
  
  key <-paste(ef,s,sep="-")
  devScatters[[key]] <- devScatter
  nodevScatters[[key]] <- nodevScatter
  
}

strategies = unique(strategies)
efs = unique(efs)

# Find maximal gain for each EF
maxGains <- rep(0, length(efs))
for(i in 1:length(efs)){
  for(j in 1:length(strategies)){
    key <-paste(efs[i],strategies[j],sep="-")
    ds <- devScatters[[key]]
    nods <- nodevScatters[[key]]
    m <- max(ds$CSGain, nods$CSGain)
    if(m > maxGains[i]){
      maxGains[i] <- m
    }
  }
}

plotScatter <- function(s, ef){
  key <-paste(ef,s,sep="-")
  ds <- devScatters[[key]]
  nods <- nodevScatters[[key]]
  minY <- min(0.8, min(ds$CSStability, nods$CSStability))
  #plot(1, type="n", xlab="", ylab="", xlim=c(0,max(ds$CSGain,nods$CSGain)), ylim=c(minY,1))
  plot(1, type="n", xlab="", ylab="", xlim=c(0,maxGains[which(ef == efs)]), ylim=c(minY,1))
  points(nods$CSGain, nods$CSStability, main="", 
         xlab="Gain", ylab="Stability", pch=1,col='blue')
  points(ds$CSGain, ds$CSStability, main="", 
       xlab="Gain", ylab="Stability", pch=4, col='red')
  
  ds$flag <- "ds"
  nods$flag <- "nods"
  d = rbind(ds, nods)
  D = d[order(d$CSGain,d$CSStability,decreasing=TRUE),]
  front = D[which(!duplicated(cummax(D$CSStability))),]
  frontDS = front[front$flag=="ds",]
  frontNODS = front[front$flag=="nods",]

if(showParetoOptimal){
  points(frontNODS$CSGain, frontNODS$CSStability, main="", 
         xlab="Gain", ylab="Stability", pch=10, cex=1, col='darkblue')
  points(frontDS$CSGain, frontDS$CSStability, main="", 
         xlab="Gain", ylab="Stability", pch=8, cex=1, col='darkred')
}
  
  grid()
  #legend('bottomleft',c('Deviation','No deviation'),pch=c(4,1),col=c('red','blue'))
}


pdf(paste(folder,'/SCATTER_PLOT.pdf',sep=""), width=10, height=15)


## setup the parameters for the device there will be 5 rows and 7 columns
## plots will fill rows first (because we used mfrow) mar sets the margins
## (bottom, left, top, right) we leave space on the bottom for an axis srt
## stands for string rotation, this is for our labels
par(mfrow = c(length(strategies) + 1, length(efs) + 1), mar = c(2, 2, 0.5, 0.8), oma=c(3,3,0,0), srt = 45)

for(si in 1:length(strategies))
{
  s <- strategies[si]
  ## Row label call plot.new() to tell R we are in a new plot set the size
  ## of the plot and fix aspect ratio at 1
  plot.new()
  plot.window(c(0, 0.5), c(0, 1), asp = 2) 
  ## add text to the plot in the center cex stands for character expansion
  ## and controls text size note text is rotated 45 degrees because of the
  ## global option set earlier in the call to par()
  stratText <-strategies[si]
  if(substr(stratText,1,5)=="Mixed")
  {
    pos <- 35 
    stratText <- paste(substr(stratText, 1, pos-1), "\n", substr(stratText, pos, nchar(stratText)), sep = "")
  }
  text(stratText, cex = 1, x = 0.5, y = 0.5)
  
  invisible(lapply(efs, plotScatter, s=s))
}

## Column lables (legend first)
par(srt=0)
plot.new()
legend("center",c('Deviation','No deviation','Deviation, PO','No deviation, PO'),pch=c(4,1,8,10),col=c('red','blue','darkred','darkblue'), cex = 1.4)
par(srt=45)
invisible(lapply(c(efs), function(i) {
  plot.new()
  plot.window(c(0, 1), c(0, 1), asp = 2)
  text(i, x = 0.5, y = 0.5, cex = 1)
}))

par(cex=2)
title(xlab = "Gain",
      ylab = "Stability",
      outer = TRUE, line = 1)

dev.off() 

