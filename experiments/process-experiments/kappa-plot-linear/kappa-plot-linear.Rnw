\documentclass{article}
\usepackage{alltt}
\begin{document}
\SweaveOpts{concordance=TRUE}
<<kappa-plot, fig.width=5, fig.height=3, dev='tikz'>>=
mean=10
gamma=1.1
coalitionSizeMax=50

coalitionSize = c(0:coalitionSizeMax)
x1 = -coalitionSize ^ gamma
#x2 = -coalitionSize ^ gamma + (coalitionSize ^ gamma)* exp(-((coalitionSize-mean)^2/(2*(std^2))))
y = -coalitionSize + mean
y[y>0] = 0

#The following code is equal to "x2 = y  ^ gamma", which R struggles with
x2 = rep(NA,length(y))       # create a vector of length equal to length of a
x2[y>=0] =  ( y[y>=0])^gamma # deal with the non-negative elements of a
x2[y< 0] = -(-y[y< 0])^gamma # deal with the negative elements of a


plot(coalitionSize,x1,type="l", xlab="$|C|$", lwd=2, ylab="penalty",ylim=c(-80,0), col='red', lty=2 ) 
grid()
lines(coalitionSize, x2, lwd=2, col='blue', lty=1 )
legend("bottomleft", c("$-|C|^{\\gamma}$","$min(-|C|+\\mu,0)^{\\gamma}$"), col=c('red','blue'), lwd=2, lty=c(2,1), bg="white") 
title(paste('Coalition size penalty, $\\mu=$ ',mean,", and $\\gamma=$",gamma, sep=""))
@
\end{document}