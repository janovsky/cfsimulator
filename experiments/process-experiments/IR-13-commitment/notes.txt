RENEWABLE PROFIT WITHOUT CF BY COMMITMENT

scenario = renewableScenario
renewableScenario.numberOfSlots = 24

renewableScenario.uncertaintyCoverage = 0.4

renewableScenario.estimateDelta = 0.2
renewableScenario.scale = 100
renewableScenario.priceGeneration = 50
renewableScenario.priceCoverUncertainty = 10
renewableScenario.priceFailureToProvide = 100
renewableScenario.commitmentWithoutCF = 0,0.2,0.4,0.6,0.8,1

simulation.numberOfCoalitionLeaders = 50
number of agents 100:100:1000