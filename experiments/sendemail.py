import smtplib
import getopt
import sys

message = ''
try:
    opts, args = getopt.getopt(sys.argv[1:], "m:h", ["message="])
except getopt.GetoptError:
    print 'Usage: sendemail.py -m <message>'
    sys.exit(2)
if not opts:
    print 'Usage: sendemail.py -m <message>'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print 'Usage: sendemail.py -m <message>'
        sys.exit()
    elif opt in ("-m", "--message"):
        message = arg

# FILL OUT THE FOLLOWING THREE LINES TO GET SENDMAIL TO WORK PROPERLY
to = ''
gmail_user = ''
gmail_pwd = ''

smtpserver = smtplib.SMTP("smtp.gmail.com", 587)
smtpserver.ehlo()
smtpserver.starttls()
smtpserver.ehlo
smtpserver.login(gmail_user, gmail_pwd)
header = 'To:' + to + '\n' + 'From: ' + gmail_user + '\n' + 'Subject:' + message + '\n'
print header
msg = header + '\n' + message + '\n\n'
smtpserver.sendmail(gmail_user, to, msg)
smtpserver.close()
