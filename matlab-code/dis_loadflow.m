function[mn] = dis_loadflow(bus_data, line_data)
% bus_data =[1 1.84  0.46
% 2 0.98  0.34
% 3 1.79  0.446
% 4 1.598  1.84];
% line_data =[1  2  0.002  0.007
% 2  3  0.0002  0.01
% 3  4  0.012  0.02
% 3  5  0.012  0.01
% ];
keyboard;
n = max(max(line_data( : , 1)), max(line_data( : , 2)));%input('number of buses = ');
[ml nl] = size(bus_data);
for k = 1 : ml
p(k) = bus_data(k, 2);Q(k) = bus_data(k, 3);
end
v1 = ones(n - 1, 1);v( : , 1) = v1;
zline = zeros(n);R = zeros(n);X = zeros(n);
[mo no] = size(line_data);
for k = 1 : mo
R(line_data(k, 1), line_data(k, 2)) = line_data(k, 3);
X(line_data(k, 1), line_data(k, 2)) = line_data(k, 4);
end
zline = R + sqrt(-1) * X;
Y = zeros(n);
for i = 1 : n
for j = 1 : n
if zline(i, j) ~= 0
Y(i, j) = 1 / zline(i, j);
Y(j, i) = Y(i, j);
end
end
end
for i = 1 : n
pc = 0;
for j = 1 : n
YB(i, i) = pc + Y(i, j);
pc = YB(i, i);
end
end
for i = 1 : n
for j = 1 : n
YBUS(i, j) = YB(i, j)-Y(i, j);
end
end
for i = 1 : n
for j = 1 : n
GL(i, j) = real(YBUS(i, j));BL(i, j) = imag(YBUS(i, j));
end
end
M_relation = zeros(n);
for i = 1 : n
for j = i : n
if i ~= j
if zline(i, j) ~= 0
M_relation(i, j) = 1;
end
end
end
end
M_relation(n, : ) = zeros(1, n);
%%%%%%%%%%%%%%%%%%%%%%%%%BIBC matrix ** ** ** ** ** ** ** ** ** ** **
a = 0;
A = zeros(n);
for i = 1 : n
for j = i : n
if i ~= j
if M_relation(i, j) == 1
a = a + 1;
if i == 1
A(a, j) = 1;
else
A( : , j) = A( : , i);
A(a, j) = 1;
end
end
end
end
end
A(n, : ) = [];A( : , 1) = [];
BIBC = A;
%%%%%%%%%%%%%%%%%%%%%%%%%%BCBV matrix ** ** ** ** ** ** ** ** ** ** **
b = 0;B = zeros(n);
for i = 1 : n
for j = i : n
if i ~= j
if M_relation(i, j) == 1
b = b + 1;
if i == 1
B(j, b) = zline(i, j);
else
B(j, : ) = B(i, : );
B(j, b) = zline(i, j);
end
end
end
end
end
B(1, : ) = [];B( : , n) = [];
BCBV = B;
DLF = BCBV * BIBC;
%%%%%%%%%%%%%%%%%%%%%start loop ** ** ** ** ** ** ** ** ** *
sd = 0;rep = 1;
while rep == 1
sd = sd + 1;
for k1 = 1 : n - 1
I_load(k1) = conj((p(k1) + sqrt(-1) * Q(k1)) / v(k1, sd));
end
CURRENT = conj(I_load)';
k = 0;
for i = 1 : n
if i == 1
V(i, sd) = 1;
else
k = k + 1 ;
V(i, sd) = v(k, sd);
end
e(i) = abs(V(i, sd));teta(i) = angle(V(i, sd));
end
delta_v = DLF * conj(I_load)';
v( :, sd + 1) = v1 - delta_v;
error = max(abs(abs(v( :, sd + 1))-abs(v( : , sd))));
if abs(error) < 1e-4
rep = 0;
voltage_AMPLI_PU = abs(v);
voltage_ANG_DEG = (180 / pi) * angle(v);
voltage_ANG_RADIAN = angle(v);
end
end
Vm =[1;voltage_AMPLI_PU( :, sd + 1)];Ang =[0;voltage_ANG_DEG( :, sd + 1)];
Ang_radian =[0;voltage_ANG_RADIAN( :, sd + 1)];

c5 = 0;c6 = 0;
for i = 1 : n
for j = 1 : n
c5 = c5 + (e(i) * e(j) * (GL(i, j) * cos(teta(i) - teta(j)) + BL(i, j) * sin(teta(i) - teta(j))));
c6 = c6 + (e(i) * e(j) * (GL(i, j) * sin(teta(i) - teta(j)) - BL(i, j) * cos(teta(i) - teta(j))));
end
P(i) = c5;Q(i) = c6;
c5 = 0;c6 = 0;
end

%p_injec = real(conj(current(1)) * Bus_Voltage(1));
fprintf('  BUS       VOL.(pu)    ANG.(Deg)       P           Q \n');
fprintf('  ______________________________________________________\n');
mn =[(1 : n)' Vm Ang P' Q'];disp(mn);
